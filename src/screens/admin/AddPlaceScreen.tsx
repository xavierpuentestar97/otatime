import {
  Alert,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import React, { useEffect, useState } from "react";
import {
  BACKGROUND,
  INPUT,
  PLACEHOLDER,
  SECONDARY_COLOR,
  globalStyles,
} from "../../theme/globalStyles";
import HeaderScreen from "../../components/header/HeaderScreen";
import { StackScreenProps } from "@react-navigation/stack";
import InputText from "../../components/input/InputText";
import { SelectList } from "react-native-dropdown-select-list";
import { AntDesign } from "@expo/vector-icons";
import { Fontisto } from "@expo/vector-icons";
import InputTextArea from "../../components/input/InputTextArea";
import BtnPrimary from "../../components/buttons/BtnPrimary";
import { getCategories } from "../../actions/category/categoryAction";
import { useQuery } from "@tanstack/react-query";
import MapCreatePlace from "../../components/map/MapCreatePlace";
import { useImage } from "../../service/hooks/useImage";
import SelectImageUser from "../../components/piker/SelectImageUser";
import {
  PlaceCreateOn,
  updatePlaceById,
} from "../../actions/place/actionPlace";
import { getPlacesById } from "../../actions/place/getPlaceById";
import { BASE_URL } from "../../service/hooks/config";
import { TouchableOpacity } from "react-native-gesture-handler";

interface Props extends StackScreenProps<any, any> {}

const AddPlaceScreen = ({ navigation, route }: Props) => {
  const id_place = route.params?.id_place;
  const [selected, setSelected] = useState(0);
  const [userPhoto, setUserPhoto] = useState("");
  const [categoryAfter, setCategoryAfter] = useState({
    key: 0,
    value: "",
  });

  const { pickImage } = useImage();

  const [form, setForm] = useState({
    name_place: "",
    description: "",
    description_en: "",
    address: "",
    lat: 0,
    lng: 0,
    category_id: 0,
  });

  const [formStyles, setFormStyles] = useState({
    name_place: styles.inputDefault,
    description: styles.inputDefault,
    description_en: styles.inputDefault,
    address: styles.inputDefault,
    userPhoto: styles.inputDefault,
    category_id: styles.inputDefault,
  });

  useEffect(() => {
    fetchPlaceData();
  }, []);

  const fetchPlaceData = async () => {
    try {
      if (id_place) {
        const placeDataArray = await getPlacesById(id_place);
        const placeData = placeDataArray[0];
        setForm(placeData);
        setUserPhoto(`${BASE_URL}${placeData.image}`);
        const category = DateCategories.filter(
          (item) => item.key === placeData.category_id
        );
        setCategoryAfter({ key: category[0].key, value: category[0].value });
        setSelected(placeData.category_id);
      } else {
        setForm({
          name_place: "",
          description: "",
          description_en: "",
          address: "",
          lat: 0,
          lng: 0,
          category_id: 0,
        });
      }
    } catch (error) {
      console.log("Error fetching place data:", error);
    }
  };

  const { data: categories = [] } = useQuery({
    queryKey: ["categories"],
    queryFn: () => getCategories(),
  });

  const DateCategories = categories.map((item) => {
    return { key: item.id_category, value: item.name_category };
  });

  const handleMarkerPress = (
    latitude: number,
    longitude: number,
    address: string
  ) => {
    setForm({
      ...form,
      address: address,
      lat: latitude,
      lng: longitude,
    });
  };

  const setDataPlace = async () => {
    if (
      !form.name_place ||
      !form.description ||
      !userPhoto ||
      !form.address ||
      !form.lat ||
      !form.lng ||
      !form.category_id
    ) {
      Alert.alert("Campos incompletos", "Por favor completa todos los campos");
      setFormStyles({
        name_place: !form.name_place ? styles.inputError : styles.inputDefault,
        description: !form.description
          ? styles.inputError
          : styles.inputDefault,
          description_en: !form.description_en
          ? styles.inputError
          : styles.inputDefault,
        address: !form.address ? styles.inputError : styles.inputDefault,
        userPhoto: !userPhoto ? styles.inputError : styles.inputDefault,
        category_id: !form.category_id
          ? styles.inputError
          : styles.inputDefault,
      });
      return;
    } else {
      setFormStyles({
        name_place: styles.inputDefault,
        description: styles.inputDefault,
        description_en: styles.inputDefault,
        address: styles.inputDefault,
        userPhoto: styles.inputDefault,
        category_id: styles.inputDefault,
      });
    }

    const formData = new FormData();
    formData.append("name_place", form.name_place);
    formData.append("description", form.description);
    formData.append("description_en", form.description_en);
    formData.append("imageUpload", userPhoto);
    formData.append("address", form.address);
    formData.append("lat", form.lat.toString());
    formData.append("lng", form.lng.toString());
    formData.append("category_id", form.category_id.toString());
    try {
      await PlaceCreateOn(formData);
      Alert.alert(
        "Lugar creado",
        "¿Qué deseas hacer?",
        [
          {
            text: "Ir",
            onPress: () => {
              navigation.replace("ListPlacesScreen");
            },
          },
        ],
        { cancelable: false }
      );
    } catch (error) {
      console.error("👏👏👏Error al crear el lugar:", error);
    }
  };

  const onUpdatePlace = async () => {
    const formData = new FormData();
    formData.append("name_place", form.name_place);
    formData.append("description", form.description);
    formData.append("description_en", form.description_en);
    formData.append("imageUpload", userPhoto);
    formData.append("address", form.address);
    formData.append("lat", form.lat.toString());
    formData.append("lng", form.lng.toString());
    formData.append("category_id", form.category_id.toString());

    const originalPlaceData = await getPlacesById(id_place);
    const originalPlace = originalPlaceData[0];

    const hasChanged =
      originalPlace.name_place !== form.name_place ||
      originalPlace.description !== form.description ||
      originalPlace.description_en !== form.description_en ||
      originalPlace.address !== form.address ||
      originalPlace.lat !== form.lat ||
      originalPlace.lng !== form.lng ||
      originalPlace.category_id !== form.category_id ||
      !!userPhoto; // Verificar si hay una nueva foto
    if (hasChanged) {
      // Realizar la actualización solo si ha habido cambios
      await updatePlaceById(id_place, formData);
      Alert.alert(
        "Lugar Actualizado",
        "Tu información ha sido actualizada correctamente"
      );
      navigation.replace("ListPlacesScreen");
    } else {
      // Si no hay cambios, regresar a la pantalla ListPlacesScreen
      navigation.replace("ListPlacesScreen");
    }
  };
  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView style={{ backgroundColor: BACKGROUND }}>
        <ScrollView
          style={{ backgroundColor: BACKGROUND }}
          showsVerticalScrollIndicator={false}
        >
          <View style={styles.container}>
            <View style={styles.headerContainer}>
              <HeaderScreen
                titleHeader="Agregar Sitio"
                funcBtnBack={() =>
                  navigation.navigate("HomeAdminScreen" as never)
                }
              />
            </View>
            <View style={styles.containerIput}>
             
              <InputText
                icon={
                  <AntDesign name="filetext1" size={24} color={PLACEHOLDER} />
                }
                text="Nombre Del Sitio"
                name="name"
                value={form.name_place}
                onChange={(name_place: string) =>
                  setForm({ ...form, name_place })
                }
                keyboardType={"default"}
                styleTextInput={{
                  ...formStyles.name_place,
                  marginTop: 15,
                  width: "90%",
                }}
              />
              <InputTextArea
                value={form.description}
                onChange={(description: string) =>
                  setForm({ ...form, description })
                }
                name="description"
                text="Descripción Del Sitio"
                style={{
                  ...formStyles.description,
                  width: "90%",
                  marginBottom: 15,
                }}
              />
              <InputTextArea
                value={form.description_en}
                onChange={(description_en: string) =>
                  setForm({ ...form, description_en })
                }
                name="description_en"
                text="Descripción Del Sitio en Ingles"
                style={{
                  ...formStyles.description,
                  width: "90%",
                  marginBottom: 15,
                }}
              />
              <View style={{ width: "90%", marginBottom: 15 }}>
                <View style={{}}>
                  <SelectImageUser
                    styleImg={{
                      ...formStyles.userPhoto,
                    }}
                    pickImage={pickImage}
                    uri={setUserPhoto}
                    imageUrl={userPhoto}
                  />
                </View>
              </View>
              <View
                style={{
                  width: "90%",
                  marginBottom: 15,
                  borderRadius: 17,
                  ...formStyles.category_id,
                }}
              >
                <SelectList
                  setSelected={setSelected}
                  data={DateCategories}
                  placeholder="Selecciona la Categoría"
                  searchPlaceholder="Buscar"
                  boxStyles={styles.select}
                  inputStyles={styles.textSelect}
                  dropdownStyles={styles.inputSelect}
                  dropdownTextStyles={styles.textIputSelect}
                  onSelect={() => {
                    setForm({ ...form, category_id: selected });
                  }}
                  defaultOption={categoryAfter}
                />
              </View>
              <InputText
                icon={<Fontisto name="map" size={24} color={PLACEHOLDER} />}
                text="Dirección Del Sitio"
                name="adress"
                value={form.address}
                onChange={(address: string) => setForm({ ...form, address })}
                keyboardType={"email-address"}
                styleTextInput={{ ...formStyles.address, width: "90%" }}
                editable={false}
              />
            </View>
            <View style={{ width: "85%", borderRadius: 16 }}>
              <MapCreatePlace
                onMarkerPress={handleMarkerPress}
                lat={form.lat}
                lng={form.lng}
              />
            </View>
            <View style={styles.containerBtn}>
              <BtnPrimary title="Registrar Sitio" func={() => id_place? onUpdatePlace() : setDataPlace()} />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default AddPlaceScreen;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: BACKGROUND,
    alignItems: "center",
    marginBottom: 20,
  },
  headerContainer: {
    width: "100%",
    alignItems: "center",
    marginBottom: 10,
  },
  containerIput: {
    width: "95%",
    borderRadius: 24,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
  },
  select: {
    height: 62,
    borderRadius: 16,
    backgroundColor: INPUT,
    justifyContent: "space-between",
    alignItems: "center",
    borderColor: BACKGROUND,
  },
  textSelect: {
    ...globalStyles.Text2,
    fontSize: 18,
    color: PLACEHOLDER,
  },
  textIputSelect: {
    ...globalStyles.Text,
    fontSize: 16,
    color: SECONDARY_COLOR,
  },
  inputSelect: {
    borderRadius: 16,
    backgroundColor: INPUT,
    borderColor: BACKGROUND,
  },
  containerBtn: {
    width: "80%",
    alignItems: "center",
    marginTop: 15,
    alignSelf: "center",
  },
  image: {
    width: "100%",
    height: 200,
    borderRadius: 16,
  },
  imgcontainer: {
    width: "100%",
    backgroundColor: INPUT,
    height: 200,
    borderRadius: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  inputError: {
    borderColor: "red", // Color de borde en caso de error
    borderWidth: 1, // Ancho de borde en caso de error
  },
  inputDefault: {},
});
