import {
  Alert,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  View,
} from "react-native";
import React, { useEffect, useState } from "react";
import { StackScreenProps } from "@react-navigation/stack";
import {
  BACKGROUND,
  SECONDARY_COLOR,
  WHITE,
  globalStyles,
} from "../../theme/globalStyles";
import HeaderScreen from "../../components/header/HeaderScreen";
import CardStateUser from "../../components/other/CardStateUser";
import BtnSecondary from "../../components/buttons/BtnSecondary";
import {
  getUserById,
  updateUserById,
  updateUserByState,
  updateUserState,
} from "../../actions/user/userAction";
import { UserUpdate } from "../../domain/entities/userUpdate";
import { getCountryDates } from "../../actions/country/countryAction";
import { Country } from "../../domain/entities/country";

interface Props extends StackScreenProps<any, any> {}

const UserStateScreen = ({ navigation }: Props) => {
  const [selectedBtn, setSelectedBtn] = useState(true);
  const [dataUser, setDataUser] = useState<UserUpdate[]>([]);
  const [dataCountry, setDataCountry] = useState<Country[]>([]);
  const [form, setForm] = useState({
    name: "",
    last_name: "",
    country_id: 0,
    age: 0,
    mail: "",
    usertype_id: 2,
    state: true,
  });

  useEffect(() => {
    const getDataUser = async () => {
      try {
        const getUser = await updateUserByState(selectedBtn ? 1 : 0);
        setDataUser(getUser);
      } catch (error) {
        console.error("Error al obtener usuarios:", error);
      }
    };
    getDataUser();
  }, [selectedBtn]);

  useEffect(() => {
    const getDataCountry = async () => {
      try {
        const getCountry = await getCountryDates();
        setDataCountry(getCountry);
      } catch (error) {
        console.error("Error al obtener paises:", error);
      }
    };
    getDataCountry();
  }, []);

  const getCountryName = (countryId: number) => {
    const country = dataCountry.find(
      (country) => country.id_country === countryId
    );
    return country ? country.name_country : "";
  };

  const fetchUserData = async (user_id: number) => {
    try {
      if (user_id) {
        const userDataArray = await getUserById(user_id);
        const userData = userDataArray[0];
        const updatedUserData = { ...userData, state: !userData.state };
        setForm(updatedUserData);
        await updateUserState(user_id, updatedUserData);
        Alert.alert("Estado del usuario actualizado correctamente");
        navigation.replace("UserStateScreen");
      } else {
        setForm({
          name: "",
          last_name: "",
          country_id: 0,
          age: 0,
          mail: "",
          usertype_id: 2,
          state: true,
        });
      }
    } catch (error) {
      console.log("Error fetching user data:", error);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView style={{ backgroundColor: BACKGROUND }}>
        <ScrollView style={{ backgroundColor: BACKGROUND }}>
          <View style={styles.container}>
            <View style={styles.headerContainer}>
              <HeaderScreen
                titleHeader="Lista de Usuarios"
                funcBtnBack={() =>
                  navigation.navigate("HomeAdminScreen" as never)
                }
              />
            </View>
            <View style={styles.containerSwich}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  width: "100%",
                }}
              >
                <View style={{ flex: 1, marginHorizontal: 8 }}>
                  <BtnSecondary
                    title="Activos"
                    enableText
                    selected={selectedBtn}
                    func={() => {
                      setSelectedBtn(!selectedBtn);
                    }}
                    styleBtn={styles.btnUnSelected}
                    styleTitle={styles.textBtnUnSelected}
                  />
                </View>
                <View style={{ flex: 1 }}>
                  <BtnSecondary
                    title="Inactivos"
                    enableText
                    selected={!selectedBtn}
                    func={() => {
                      setSelectedBtn(!selectedBtn);
                    }}
                    styleBtn={styles.btnUnSelected}
                    styleTitle={styles.textBtnUnSelected}
                  />
                </View>
              </View>
            </View>
            <View style={styles.containerIput}>
              {dataUser.map((item, index) => (
                <CardStateUser
                  userText={`${item.name} ${item.last_name}`}
                  textCountry={getCountryName(item.country_id)}
                  textMail={item.mail}
                  stateUser={item.state}
                  key={index}
                  onPress={() => fetchUserData(item.id_user)}
                />
              ))}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default UserStateScreen;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: BACKGROUND,
    alignItems: "center",
  },
  headerContainer: {
    width: "100%",
  },
  containerIput: {
    width: "90%",
    borderRadius: 24,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  containerSwich: {
    width: "90%",
    borderRadius: 24,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  btnUnSelected: {
    width: "100%",
    height: 40,
    borderRadius: 24,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: SECONDARY_COLOR,
  },
  textBtnUnSelected: {
    fontSize: 16,
    color: WHITE,
    ...globalStyles.TitleSecondary,
    marginHorizontal: 10,
  },
});
