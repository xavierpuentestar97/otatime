import { SafeAreaView, ScrollView, StyleSheet, Text, View } from "react-native";
import React from "react";
import {
  BACKGROUND,
  PRIMARY_COLOR,
  SECONDARY_COLOR,
  WHITE,
  globalStyles,
} from "../../theme/globalStyles";
import HeaderScreen from "../../components/header/HeaderScreen";
import { StackScreenProps } from "@react-navigation/stack";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import CardBtn from "../../components/other/CardBtn";
import CircleBtn from "../../components/buttons/CircleBtn";
import { Entypo } from "@expo/vector-icons";
import { useAuthPlace } from "../../presentation/useAuthPlace";
import CardBtnHome from "../../components/other/CardBtnHome";

interface Props extends StackScreenProps<any, any> {}

const HomeAdminScreen = ({ navigation }: Props) => {
  const { logOut } = useAuthPlace();
  const { user } = useAuthPlace();

  const data = [
    {
      title: "Crear Sitio",
      imgUrl: require("../../assets/images/add_place.png"),
    },
    {
      title: "Usuarios",
      imgUrl: require("../../assets/images/user_card.png"),
    },
    {
      title: "Sitios Turísticos",
      imgUrl: require("../../assets/images/place_tourist.png"),
    },
    {
      title: "Comentarios",
      imgUrl: require("../../assets/images/comment_card.png"),
    },
  ];
  const OnpressLogOut = async () => {
    await logOut();
    navigation.navigate("LoginScreen");
  };

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }}>
        <ScrollView style={{ backgroundColor: BACKGROUND }}>
          <View style={styles.container}>
            <View style={styles.headerContainer}>
              <HeaderScreen
                titleHeader="Inicio"
                funcBtnBack={() =>{}}
              />
            </View>
            <View style={styles.useContainer}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={styles.textWelcome}>Bienvenido</Text>
                <MaterialCommunityIcons
                  name="hand-wave"
                  size={12}
                  color={SECONDARY_COLOR}
                />
              </View>
                <Text style={styles.textUser}>{user?.user}</Text>
              <View
                style={{
                  alignItems: "flex-end",
                  zIndex:1,
                  position:'absolute',
                  right: 10,
                }}
              >
                <CircleBtn
                  styleBtn={styles.btnOutLogin}
                  icon={<Entypo name="log-out" size={20} color={WHITE} />}
                  func={OnpressLogOut}
                />
              </View>
            </View>

            <View style={styles.containerIput}>
              <CardBtnHome
                title={data[0].title}
                imageSource={data[0].imgUrl}
                styleCard={{ marginBottom: 9 }}
                onPressCard={() => {
                  navigation.navigate("AddPlaceScreen");
                }}
              />
              <CardBtnHome
                title={data[1].title}
                imageSource={data[1].imgUrl}
                styleCard={{ marginBottom: 9 }}
                onPressCard={() => {
                  navigation.navigate("UserStateScreen");
                }}
              />
              <CardBtnHome
                title={data[2].title}
                imageSource={data[2].imgUrl}
                styleCard={{ marginBottom: 9 }}
                onPressCard={() => {
                  navigation.navigate("ListPlacesScreen");
                }}
              />
              <CardBtnHome
                title={data[3].title}
                imageSource={data[3].imgUrl}
                styleCard={{ marginBottom: 9 }}
                onPressCard={() => {
                  navigation.navigate("PlacesCommentScreen");
                }}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default HomeAdminScreen;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: BACKGROUND,
    alignItems: "center",
  },
  headerContainer: {
    width: "100%",
    height: 100,
    alignItems: "center",
    backgroundColor: PRIMARY_COLOR,
    borderBottomRightRadius: 24,
    borderBottomLeftRadius: 24,
  },
  useContainer: {
    width: "90%",
    height: 54,
    justifyContent: "center",
    marginTop: 20,
    // backgroundColor:'red'
  },
  textWelcome: {
    fontSize: 15,
    ...globalStyles.Text2,
    color: SECONDARY_COLOR,
    marginRight: 2,
  },
  containerIput: {
    width: "90%",
    borderRadius: 24,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 25,
  },
  btnOutLogin: {
    width: 35,
    height: 35,
  },
  textUser: {
    fontSize: 20,
    ...globalStyles.Text,
    color: SECONDARY_COLOR,
    marginTop: 1,
    marginRight: 2,
  },
});
