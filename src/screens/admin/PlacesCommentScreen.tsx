import { SafeAreaView, ScrollView, StyleSheet, Text, View } from "react-native";
import React, { useEffect, useState } from "react";
import { StackScreenProps } from "@react-navigation/stack";
import {
  BACKGROUND,
  INPUT,
  PLACEHOLDER,
  PRIMARY_COLOR,
  SECONDARY_COLOR,
  WHITE,
  globalStyles,
} from "../../theme/globalStyles";
import HeaderScreen from "../../components/header/HeaderScreen";
import CardBtn from "../../components/other/CardBtn";
import { getAllPlaceComment } from "../../actions/place/placeAction";
import { PlaceResponse } from "../../infrastructure/interfaces/places.response";
import { getCategories } from "../../actions/category/categoryAction";
import { Category } from "../../domain/entities/category";
import { BASE_URL } from "../../service/hooks/config";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AntDesign } from "@expo/vector-icons";

interface Props extends StackScreenProps<any, any> {}

const PlacesCommentScreen = ({ navigation }: Props) => {
  const [dataPlaces, setdataPlaces] = useState<PlaceResponse[]>([]);
  const [dataCategory, setDataCategory] = useState<Category[]>([]);
  const [page, setPage] = useState(1);

  useEffect(() => {
    DataCommentPlace(page);
  }, [page]);

  useEffect(() => {
    const getDataCategory = async () => {
      try {
        const getCategory = await getCategories();
        setDataCategory(getCategory);
      } catch (error) {
        console.error("Error al obtener categorias:", error);
      }
    };
    getDataCategory();
  }, []);

  const getCategoryName = (categoryId: number) => {
    const category = dataCategory.find(
      (category) => category.id_category === categoryId
    );
    return category ? category.name_category : "";
  };

  const DataCommentPlace = async (pageNumber: number) => {
    try {
      const response = await getAllPlaceComment(pageNumber);
      setdataPlaces(response);
    } catch (error) {
      console.log(error);
    }
  };
  const goToNextPage = () => {
    setPage(page + 1);
  };

  const goToPreviousPage = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView style={{ backgroundColor: BACKGROUND }}>
        <ScrollView style={{ backgroundColor: BACKGROUND }}>
          <View style={styles.container}>
            <View style={styles.headerContainer}>
              <HeaderScreen
                titleHeader="Lista de Sitios Comentados"
                funcBtnBack={() =>
                  navigation.navigate("HomeAdminScreen" as never)
                }
              />
            </View>

            <View style={styles.containerIput}>
              <View style={styles.containerPagination}>
                <TouchableOpacity
                  style={styles.btnPagination}
                  onPress={goToPreviousPage}
                >
                  <AntDesign name="leftcircleo" size={20} color={WHITE} />
                </TouchableOpacity>
                <Text style={styles.textPagination}>Pagina: {page}</Text>
                <TouchableOpacity
                  style={styles.btnPagination}
                  onPress={goToNextPage}
                >
                  <AntDesign name="rightcircleo" size={20} color={WHITE} />
                </TouchableOpacity>
              </View>
              {dataPlaces.map((item, index) => (
                <CardBtn
                  imageSource={`${BASE_URL}${item.image}`}
                  dateText
                  categoryText={getCategoryName(item.category_id)}
                  directionText={`${item.lat} ${item.lng}`}
                  title={item.name_place}
                  styleTitle={styles.title}
                  styleCard={styles.card}
                  styleImg={styles.img}
                  key={index}
                  onPressCard={() =>
                    navigation.navigate("CommentsPlaceAllScreen", {
                      id_place: item.id_place,
                    })
                  }
                />
              ))}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default PlacesCommentScreen;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: BACKGROUND,
    alignItems: "center",
  },
  headerContainer: {
    width: "100%",
  },
  containerIput: {
    width: "90%",
    borderRadius: 24,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  title: {
    fontSize: 18,
    color: SECONDARY_COLOR,
    textAlign: "left",
  },
  card: {
    backgroundColor: INPUT,
    alignItems: "center",
    marginBottom: 15,
  },
  img: {
    width: 130,
    height: 130,
    borderRadius: 16,
    marginHorizontal: 10,
  },
  containerPagination: {
    width: "100%",
    // backgroundColor:'pink',
    marginTop: 10,
    marginBottom: 15,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  btnPagination: {
    width: 35,
    height: 35,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: PRIMARY_COLOR,
    marginHorizontal: 10,
  },
  textPagination: {
    fontSize: 18,
    color: PLACEHOLDER,
    ...globalStyles.Text,
  },
});
