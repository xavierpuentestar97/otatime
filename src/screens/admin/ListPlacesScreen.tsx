import {
  Alert,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  View,
} from "react-native";
import React, { useEffect, useState } from "react";
import {
  BACKGROUND,
  INPUT,
  SECONDARY_COLOR,
  WHITE,
  globalStyles,
} from "../../theme/globalStyles";
import HeaderScreen from "../../components/header/HeaderScreen";
import { StackScreenProps } from "@react-navigation/stack";
import BtnPrimary from "../../components/buttons/BtnPrimary";
import { getPlaces, getPlacesByName } from "../../actions/place/placeAction";
import { Place } from "../../domain/entities/place";
import InputText from "../../components/input/InputText";
import CircleBtn from "../../components/buttons/CircleBtn";
import { Feather } from "@expo/vector-icons";
import { BASE_URL } from "../../service/hooks/config";
import CardBtnPlace from "../../components/other/CardBtnPlace";
import { PlaceDetele } from "../../actions/place/actionPlace";

interface Props extends StackScreenProps<any, any> {}

const ListPlacesScreen = ({ navigation }: Props) => {
  const [useSearch, setUseSearch] = useState(false);
  const [places, setPlaces] = useState<Place[]>([]);
  const [searchValue, setSearchValue] = useState("");

  useEffect(() => {
    const DataSearchPlace = async () => {
      try {
        const data = searchValue
          ? await getPlacesByName(searchValue)
          : await getPlaces();
        setPlaces(data);
      } catch (error) {
        console.error("Error al obtener lugares:", error);
      }
    };
    DataSearchPlace();
  }, [searchValue]);

  const onPressSearch = async () => {
    if (searchValue.length > 0) {
      setUseSearch(true);
    }
  };

  const BtnDeletePlace = async (placeId: number) => {
    try {
      Alert.alert(
        "Confirmación",
        "¿Estás seguro de que quieres eliminar este sitio?",
        [
          {
            text: "No",
            style: "cancel",
          },
          {
            text: "Sí",
            onPress: async () => {
              await PlaceDetele(placeId);
              const fetchData = async () => {
                try {
                  const data = useSearch
                    ? await getPlacesByName(searchValue)
                    : await getPlaces();
                  setPlaces(data);
                } catch (error) {
                  console.error("Error al obtener lugares:", error);
                }
              };
              fetchData();
            },
          },
        ],
        { cancelable: false }
      );
    } catch (error) {
      console.error("Error al eliminar el lugar:", error);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView style={{ backgroundColor: BACKGROUND }}>
        <ScrollView style={{ backgroundColor: BACKGROUND }}>
          <View style={styles.container}>
            <View style={styles.headerContainer}>
              <HeaderScreen
                titleHeader="Lista de Sitios"
                funcBtnBack={() =>
                  navigation.navigate("HomeAdminScreen" as never)
                }
              />
            </View>

            <View style={styles.containerInput}>
              <InputText
                name="search"
                value={searchValue}
                onChange={setSearchValue}
                keyboardType={"web-search"}
                styleTextInput={styles.inputSearch}
              />
              <CircleBtn
                icon={<Feather name="search" size={24} color={WHITE} />}
                styleBtn={styles.btnSearch}
                func={onPressSearch}
              />
            </View>
            <View style={styles.containerBtn}>
              <BtnPrimary
                func={() => navigation.navigate("AddPlaceScreen")}
                title="Añadir Sitio"
                styleBtn={{ width: "80%", marginTop: 15, borderRadius: 32 }}
              />
            </View>
            <View style={styles.containerIput}>
              {places.map((item, index) => {
                return (
                <CardBtnPlace
                  imageSource={`${BASE_URL}${item.image}`}
                  dateText
                  textLatitud={item.lat}
                  textLongitud={item.lng}
                  directionText={item.address}
                  title={item.name_place}
                  styleTitle={styles.title}
                  styleCard={styles.card}
                  styleImg={styles.img}
                  key={index}
                  onPressBtn={() => BtnDeletePlace(item.id_place)}
                  onPressCard={()=>{
                    navigation.navigate("AddPlaceScreen", {
                      id_place: item.id_place,
                    });
                  }}
                />
              )
              })}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default ListPlacesScreen;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: BACKGROUND,
    alignItems: "center",
  },
  headerContainer: {
    width: "100%",
  },
  containerIput: {
    width: "90%",
    borderRadius: 24,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
  },
  containerSwich: {
    width: "90%",
    borderRadius: 24,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  btnUnSelected: {
    width: "100%",
    height: 40,
    borderRadius: 24,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: SECONDARY_COLOR,
  },
  textBtnUnSelected: {
    fontSize: 16,
    color: WHITE,
    ...globalStyles.TitleSecondary,
    marginHorizontal: 10,
  },

  title: {
    fontSize: 18,
    color: SECONDARY_COLOR,
    textAlign: "left",
  },
  card: {
    backgroundColor: INPUT,
    alignItems: "center",
    marginBottom: 15,
  },
  img: {
    width: 130,
    height: 130,
    borderRadius: 16,
    marginHorizontal: 10,
  },
  containerBtn: {
    width: "100%",
    alignItems: "center",
  },
  containerInput: {
    width: "90%",
    backgroundColor: INPUT,
    height: 62,
    flexDirection: "row",
    justifyContent: "center",
    borderRadius: 32,
    marginTop: 20,
  },
  inputSearch: {
    flex: 1,
    borderRadius: 32,
  },
  btnSearch: {
    alignSelf: "center",
    marginRight: 6,
  },
});
