import { SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { StackScreenProps } from '@react-navigation/stack'
import { BACKGROUND, INPUT, PLACEHOLDER, PRIMARY_COLOR, SECONDARY_COLOR, WHITE, globalStyles } from '../../theme/globalStyles';
import HeaderScreen from '../../components/header/HeaderScreen';
import CardComment from '../../components/other/CardComment';
import { CommentUser } from '../../domain/entities/comment';
import { getCommentById } from '../../actions/comment/commetAction';
import { AntDesign } from "@expo/vector-icons";
import { getCommentByPlace } from '../../actions/place/placeAction';

interface Props extends StackScreenProps<any, any> {}

const CommentsPlaceAllScreen = ({ navigation, route }: Props) => {
  const [comment, setComment] = useState<CommentUser[]>([]);
  const [page, setPage] = useState(1);

  const id_place = route.params?.id_place;

  useEffect(() => {
    
    CommentPlace(page);
  }, [page,id_place]);

  const CommentPlace = async (pageNumber: number) => {
    try {
      const commentData = await getCommentByPlace(id_place, pageNumber);
      setComment(commentData);
    } catch (error) {
      console.error("Error al obtener comentarios:", error);
    }
  };

  const goToNextPage = () => {
    setPage(page + 1);
  };

  const goToPreviousPage = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };
  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView style={{ backgroundColor: BACKGROUND }}>
        <ScrollView style={{ backgroundColor: BACKGROUND }}>
          <View style={styles.container}>
            <View style={styles.headerContainer}>
              <HeaderScreen
                titleHeader="Lista de Comentados"
                funcBtnBack={() =>
                  navigation.navigate("PlacesCommentScreen" as never)
                }
              />
            </View>

            <View style={styles.containerIput}>
            <View style={styles.containerPagination}>
                <TouchableOpacity
                  style={styles.btnPagination}
                  onPress={goToPreviousPage}
                >
                  <AntDesign name="leftcircleo" size={20} color={WHITE} />
                </TouchableOpacity>
                <Text style={styles.textPagination}>Pagina: {page}</Text>
                <TouchableOpacity
                  style={styles.btnPagination}
                  onPress={goToNextPage}
                >
                  <AntDesign name="rightcircleo" size={20} color={WHITE} />
                </TouchableOpacity>
              </View>
              {comment.map((item, index) => (
               <CardComment
               key={index}
               userComment={item.user_name}
               lastName={item.user_last_name}
               comment={item.content}
               userCalification={item.rate}
             />
              ))}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  )
}

export default CommentsPlaceAllScreen

const styles = StyleSheet.create({
    container: {
        width: "100%",
        backgroundColor: BACKGROUND,
        alignItems: "center",
      },
      headerContainer: {
        width: "100%",
      },
      containerIput: {
        width: "90%",
        borderRadius: 24,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 20,
      },
       title: {
        fontSize: 18,
        color: SECONDARY_COLOR,
        textAlign: "left",
      },
      card: {
        backgroundColor: INPUT,
        alignItems: "center",
        marginBottom: 15,
      },
      img: {
        width: 130,
        height: 130,
        borderRadius: 16,
        marginHorizontal: 10,
      },
      btnPagination: {
        width: 35,
        height: 35,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: PRIMARY_COLOR,
        marginHorizontal: 10,
      },
      textPagination: {
        fontSize: 18,
        color: PLACEHOLDER,
        ...globalStyles.Text,
      },
      containerPagination: {
        width: "100%",
        // backgroundColor:'pink',
        marginTop: 10,
        marginBottom: 15,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
      },
})