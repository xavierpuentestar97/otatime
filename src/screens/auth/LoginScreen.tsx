import { StyleSheet, Text, View, ScrollView, TouchableOpacity, SafeAreaView, Alert } from 'react-native'
import React, { useEffect, useState } from 'react'
import { StackScreenProps } from '@react-navigation/stack'
import LogoContainer from '../../components/ContainerLogo/LogoContainer'
import BtnPrimary from '../../components/buttons/BtnPrimary'
import { BACKGROUND, INPUT, PLACEHOLDER, PRIMARY_COLOR, SECONDARY_COLOR, WHITE, globalStyles } from '../../theme/globalStyles'
import HeaderScreen from '../../components/header/HeaderScreen'
import InputText from '../../components/input/InputText'
import { AntDesign } from '@expo/vector-icons'
import { Feather } from '@expo/vector-icons'
import { useAuthPlace } from '../../presentation/useAuthPlace'
import i18n from '../../i18next/i18n.config'



interface Props extends StackScreenProps<any, any> {}


const LoginScreen = ({ route,navigation }: Props) => {

  const email = route.params?.email;
  const password = route.params?.password;
  const {login} = useAuthPlace();
  const [isPosting, setIsPosting] = useState(false)

  const [form, setForm] = useState({
    mail:  '',
    password:  ''
  })


const onLogin = async() =>{
  if(form.mail.length ===0 || form.password.length ===0){
    return;
  }
  setIsPosting(true)
  const wasSuccessFull = await login(form.mail, form.password)
  setIsPosting(false)
  if(wasSuccessFull) return
  Alert.alert('Error', i18n.t('alertLogin'))
}

  return (
    <View style={{flex:1 }}>
        <SafeAreaView style={{backgroundColor: PRIMARY_COLOR}}>
        <ScrollView style={{backgroundColor:BACKGROUND,}}>
      <View style={styles.container}>

      <View style={styles.headerContainer}>
        <HeaderScreen titleHeader={i18n.t('titleLogin')}
        funcBtnBack={()=> navigation.goBack()}
        />
        </View>
      <View style={styles.containerLogo}>
      <LogoContainer styleImg={styles.img}/>
      </View>
      <View style={styles.containerIput}>
       <InputText
       icon={<AntDesign name="user" size={24} color={PLACEHOLDER} />}
       text={i18n.t('textInputMail')}
       name='email'
       value={form.mail}
       onChange={(mail:string)=>setForm({...form, mail})}
       keyboardType={"email-address"}
       styleTextInput={{marginTop:15, width:'90%'}}
       />
       <InputText
       icon={<Feather name="key" size={24} color={PLACEHOLDER} />}
       text={i18n.t('textInputPass')}
       name='password'
       value={form.password}
       onChange={(password:string)=> setForm({...form, password})}
       secureTextEntry
       styleTextInput={{width: '90%'}}
       />
      <TouchableOpacity style={{marginBottom:15}}
      onPress={()=>navigation.navigate('ForgotPassScreen')}
      >
      <Text style={styles.textUnderLine}>{i18n.t('btnForgotPass')}</Text>
      </TouchableOpacity>
      </View>
      </View>

      <View style={styles.containerBtn}>
      <BtnPrimary 
      title={i18n.t('titleLogin')}
      func={onLogin}
      disableBtn={isPosting}
      />
      
      <BtnPrimary 
      title={i18n.t('btnRegister')}
      func={() =>{ 
        
        navigation.navigate("SignUpScreen")
      }}
      styleBtn={{backgroundColor: BACKGROUND,}}
      styleTitle={styles.textBtn}
      />
      </View>

      </ScrollView>
      </SafeAreaView>
    </View>
  )
}

export default LoginScreen

const styles = StyleSheet.create({
    container:{
        width: '100%',
        backgroundColor: BACKGROUND,
        alignItems: 'center',
      },
      containerBtn:{
        width:'80%',
        alignItems: 'center',   
        marginTop:70,
        alignSelf:'center',
      },
      containerLogo:{
        width:230,
        height:230,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: INPUT,
        borderRadius:150,
        position:'absolute',
        marginTop:80
      },
      
      headerContainer:{
        width: '100%',
        height: 210,
        alignItems: 'center',
        backgroundColor: PRIMARY_COLOR,
        borderBottomRightRadius:24,
        borderBottomLeftRadius:24,
      },
      img:{
        width:120,
        height:170,
      },
      containerIput:{
        width: '80%',
        backgroundColor: WHITE,
        borderRadius: 24,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:120
      },
      textUnderLine:{
        textDecorationLine: 'underline',
        ...globalStyles.Text2 
      },
      textBtn:{
        textDecorationLine: 'underline',
        ...globalStyles.Text2,
        color: SECONDARY_COLOR,
        fontSize: 15

      },
      textBtnInvitado:{
        
        color: SECONDARY_COLOR
      }
})