import { Alert, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useState } from 'react'
import { BACKGROUND, INPUT, PLACEHOLDER, PRIMARY_COLOR, SECONDARY_COLOR, WHITE, globalStyles } from '../../theme/globalStyles'
import HeaderScreen from '../../components/header/HeaderScreen'
import { StackScreenProps } from '@react-navigation/stack'
import LogoContainer from '../../components/ContainerLogo/LogoContainer'
import InputText from '../../components/input/InputText'
import BtnPrimary from '../../components/buttons/BtnPrimary'
import { AntDesign } from '@expo/vector-icons'
import { sendEmail } from '../../actions/auth/forgotPassword'
import i18n from '../../i18next/i18n.config'


interface Props extends StackScreenProps<any, any> {}

const ForgotPassScreen = ({ route,navigation }: Props) => {
    const [error, setError] = useState('');
    const [form, setForm] = useState({
        mail: "",
      });
const onSendEmail = async() =>{
if(!form.mail) {
    setError(i18n.t('alertInput'));
    return;
}
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(form.mail)) {
        setError("El correo electrónico no es válido");
        return;
    }
    const wasSuccessFull = await sendEmail(
        form.mail,
      );
      if (!wasSuccessFull) {
        Alert.alert("Error", i18n.t('alertMailError'));
      }else{
        Alert.alert(i18n.t('alertMail'));
        navigation.navigate("LoginScreen");
      }
}

  return (
    <View style={{flex:1 }}>
    <SafeAreaView style={{backgroundColor: PRIMARY_COLOR}}>
    <ScrollView style={{backgroundColor:BACKGROUND,}}>
  <View style={styles.container}>

  <View style={styles.headerContainer}>
    <HeaderScreen titleHeader={i18n.t('titleForgotPass')}
    funcBtnBack={()=> navigation.goBack()}
    />
    </View>
  <View style={styles.containerLogo}>
  <LogoContainer styleImg={styles.img}/>
  </View>
  <View style={styles.containerIput}>
    <View style={{marginTop:15, width:'85%'}}>
    <Text style={styles.textContainer}>{i18n.t('labelSendMail')}</Text>
    </View>
   <InputText
   icon={<AntDesign name="user" size={24} color={PLACEHOLDER} />}
   text={i18n.t('textInputMail')}
   name='email'
   value={form.mail}
   onChange={(mail: string) => setForm({ ...form, mail })}
   keyboardType={"email-address"}
   styleTextInput={{marginTop:15, width:'90%'}}
   />
   <BtnPrimary 
  title={i18n.t('btnSend')}
  func={onSendEmail}
  styleBtn={{width:'90%'}}

  />
  </View>
  
  </View>

  </ScrollView>
  </SafeAreaView>
</View>
  )
}

export default ForgotPassScreen

const styles = StyleSheet.create({
    container:{
        width: '100%',
        backgroundColor: BACKGROUND,
        alignItems: 'center',
      },
      
      containerLogo:{
        width:230,
        height:230,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: INPUT,
        borderRadius:150,
        position:'absolute',
        marginTop:80
      },
      
      headerContainer:{
        width: '100%',
        height: 210,
        alignItems: 'center',
        backgroundColor: PRIMARY_COLOR,
        borderBottomRightRadius:24,
        borderBottomLeftRadius:24,
      },
      img:{
        width:120,
        height:170,
      },
      containerIput:{
        width: '80%',
        backgroundColor: WHITE,
        borderRadius: 24,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:150
      },
      textContainer:{
        ...globalStyles.Text2,
        color: SECONDARY_COLOR,
        fontSize: 15,
        textAlign: 'left',
      }
     
})