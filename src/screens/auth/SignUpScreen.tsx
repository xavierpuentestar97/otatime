import {
  Alert,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import React, { useEffect, useState } from "react";
import {
  BACKGROUND,
  CARDHOME,
  INPUT,
  PLACEHOLDER,
  PRIMARY_COLOR,
  SECONDARY_COLOR,
  globalStyles,
} from "../../theme/globalStyles";
import HeaderScreen from "../../components/header/HeaderScreen";
import { StackScreenProps } from "@react-navigation/stack";
import InputText from "../../components/input/InputText";
import { AntDesign } from "@expo/vector-icons";
import { Feather } from "@expo/vector-icons";
import { FontAwesome5 } from "@expo/vector-icons";
import { Fontisto } from "@expo/vector-icons";
import { SelectList } from "react-native-dropdown-select-list";
import { MaterialIcons } from "@expo/vector-icons";
import BtnPrimary from "../../components/buttons/BtnPrimary";
import { getCountryDates } from "../../actions/country/countryAction";
import { useQuery } from "@tanstack/react-query";
import { useCreateUser } from "../../presentation/useCreateUser";
import { getUserById, updateUserById } from "../../actions/user/userAction";
import { UserUpdate } from "../../domain/entities/userUpdate";
import i18n from "../../i18next/i18n.config";
import { StorageAdapter } from "../../config/adapters/storage-adapter";

interface Props extends StackScreenProps<any, any> {}

const SignUpScreen = ({ navigation, route }: Props) => {
  const [selected, setSelected] = useState(0);
  const { createUser } = useCreateUser();
  const [error, setError] = useState("");
  const id_user = route.params?.id_user;

  const [form, setForm] = useState({
    name: "",
    last_name: "",
    country_id: 0,
    age: 0,
    mail: "",
    password: "",
    usertype_id: 2,
    state: true,
  });

  const fetchUserData = async () => {
    try {
      const pass = await StorageAdapter.getItem('passwordStorage')
      console.log(pass)
      if (id_user) {
        const userDataArray = await getUserById(id_user);
        const userData = userDataArray[0];
        setForm({...userData, password: pass!});
      } else {
        setForm({
          name: "",
          last_name: "",
          country_id: 0,
          age: 0,
          mail: "",
          password: "",
          usertype_id: 2,
          state: true,
        });
      }
    } catch (error) {
      console.log("Error fetching user data:", error);
    }
  };

  useEffect(() => {
    fetchUserData();
  }, []);

  const userDataToUpdate: UserUpdate = {
    id_user: id_user,
    name: form.name,
    last_name: form.last_name,
    country_id: form.country_id,
    age: form.age,
    mail: form.mail,
    password: form.password,
    usertype_id: form.usertype_id,
    state: form.state,
  };

  const onCreated = async () => {
    if (
      !form.name ||
      !form.last_name ||
      !form.country_id ||
      !form.age ||
      !form.mail ||
      !form.password
    ) {
      setError(i18n.t("alertInput"));
      return;
    }
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(form.mail)) {
      setError(i18n.t("AlertMailFail"));
      return;
    }
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
    if (!passwordRegex.test(form.password)) {
      setError(i18n.t("AlertValdationPass"));
      return;
    }
    const wasSuccessFull = await createUser(
      form.name,
      form.last_name,
      form.country_id,
      form.age,
      form.mail,
      form.password,
      form.usertype_id,
      form.state
    );
    if (!wasSuccessFull) {
      Alert.alert("Error", "No se pudo crear el usuario");
    } else {
      navigation.navigate("LoginScreen", {
        email: form.mail,
        password: form.password,
      });
    }
  };

  const onUpdateUser = async () => {
    updateUserById(id_user, userDataToUpdate);
    Alert.alert(
      i18n.t("AlertUpdateUser"),
      i18n.t("AlertUpdateUser2")
    );
    navigation.navigate("LoginScreen", {
      email: form.mail,
      password: form.password,
    });
  };
  const { isLoading, data: countries = [] } = useQuery({
    queryKey: ["countries", "infinite"],
    staleTime: 1000 * 60 * 60, // 1 hora
    queryFn: () => getCountryDates(),
  });

  const DateCountries = countries.map((item) => {
    return { key: item.id_country, value: item.name_country };
  });
  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }}>
        <ScrollView style={{ backgroundColor: BACKGROUND }}>
          <View style={styles.container}>
            <View style={styles.headerContainer}>
              <HeaderScreen
                titleHeader={i18n.t("titleRegister")}
                funcBtnBack={() => navigation.goBack()}
              />
            </View>
            <View style={styles.containerLogo}>
              <FontAwesome5 name="user-alt" size={50} color={SECONDARY_COLOR} />
            </View>

            <View style={styles.containerIput}>
              <InputText
                icon={<AntDesign name="user" size={24} color={PLACEHOLDER} />}
                text={i18n.t("textInputName")}
                name="name"
                value={form.name}
                onChange={(name: string) => setForm({ ...form, name })}
                keyboardType={"default"}
                styleTextInput={{ marginTop: 15, width: "90%" }}
              />

              <InputText
                icon={<AntDesign name="user" size={24} color={PLACEHOLDER} />}
                text={i18n.t("textInputLastN")}
                name="last_name"
                value={form.last_name}
                onChange={(last_name: string) =>
                  setForm({ ...form, last_name })
                }
                keyboardType={"default"}
                styleTextInput={{ width: "90%" }}
              />
              <InputText
                icon={
                  <Fontisto name="calendar" size={24} color={PLACEHOLDER} />
                }
                text="Edad"
                name="age"
                value={form.age.toString()}
                onChange={(text: string) => {
                  const age = parseInt(text, 10);
                  if (!isNaN(age)) {
                    setForm({ ...form, age });
                  }
                }}
                keyboardType={"number-pad"}
                styleTextInput={{ width: "90%" }}
              />

              <View style={{ width: "90%", marginBottom: 15 }}>
                <SelectList
                  setSelected={setSelected}
                  data={DateCountries}
                  placeholder={i18n.t("textInputCountry")}
                  searchPlaceholder={i18n.t("SearchCountry")}
                  boxStyles={styles.select}
                  inputStyles={styles.textSelect}
                  dropdownStyles={styles.inputSelect}
                  dropdownTextStyles={styles.textIputSelect}
                  onSelect={() => {
                    setForm({ ...form, country_id: selected });
                  }}
                />
              </View>
              <InputText
                icon={
                  <MaterialIcons
                    name="alternate-email"
                    size={24}
                    color={PLACEHOLDER}
                  />
                }
                text={i18n.t("textInputMail")}
                name="email"
                value={form.mail}
                onChange={(mail: string) => setForm({ ...form, mail })}
                keyboardType={"email-address"}
                styleTextInput={{ width: "90%" }}
              />
              <InputText
                icon={<Feather name="key" size={24} color={PLACEHOLDER} />}
                text={i18n.t("textInputPass")}
                name="password"
                value={form.password}
                onChange={(password: string) => setForm({ ...form, password })}
                secureTextEntry
                styleTextInput={{ width: "90%" }}
              />
            </View>
            <View style={{ width: "80%" }}>
              {error && <Text style={styles.error}>{error}</Text>}
            </View>
            <View style={styles.containerBtn}>
              <BtnPrimary
                title={i18n.t("BtnRegister")}
                func={() => (id_user ? onUpdateUser() : onCreated())}
              />
              <BtnPrimary
                title={i18n.t("BtnLogin")}
                func={() => {
                  navigation.goBack();
                }}
                styleBtn={{ backgroundColor: BACKGROUND }}
                styleTitle={styles.textBtn}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default SignUpScreen;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: BACKGROUND,
    alignItems: "center",
  },
  headerContainer: {
    width: "100%",
    height: 150,
    alignItems: "center",
    backgroundColor: PRIMARY_COLOR,
    borderBottomRightRadius: 24,
    borderBottomLeftRadius: 24,
  },
  containerLogo: {
    width: 110,
    height: 110,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: INPUT,
    borderRadius: 150,
    position: "absolute",
    marginTop: 80,
  },
  containerIput: {
    width: "90%",
    borderRadius: 24,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 60,
  },
  select: {
    height: 62,
    borderRadius: 16,
    backgroundColor: INPUT,
    justifyContent: "space-between",
    alignItems: "center",
    borderColor: BACKGROUND,
  },
  textSelect: {
    ...globalStyles.Text2,
    fontSize: 18,
    color: PLACEHOLDER,
  },
  textIputSelect: {
    ...globalStyles.Text,
    fontSize: 16,
    color: SECONDARY_COLOR,
  },
  inputSelect: {
    borderRadius: 16,
    backgroundColor: INPUT,
    borderColor: BACKGROUND,
  },
  containerBtn: {
    width: "80%",
    alignItems: "center",
    marginTop: 40,
    alignSelf: "center",
  },
  textBtn: {
    textDecorationLine: "underline",
    ...globalStyles.Text2,
    color: SECONDARY_COLOR,
    fontSize: 15,
  },
  error: {
    ...globalStyles.Text2,
    color: CARDHOME,
    fontSize: 13,
    textAlign: "center",
  },
});
