import { Dimensions, FlatList, Image, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import { BACKGROUND, PLACEHOLDER, PRIMARY_COLOR, SECONDARY_COLOR, WHITE, globalStyles } from '../../theme/globalStyles'
import { TouchableOpacity } from 'react-native-gesture-handler'
import BtnPrimary from '../../components/buttons/BtnPrimary'
import { StackScreenProps } from '@react-navigation/stack'
import i18n from '../../i18next/i18n.config'

const {width} = Dimensions.get('window')
interface Props extends StackScreenProps<any, any> {}

interface CarouselDataItem {
  title: string
  body: string
  imgUrl: number
}

const SliderScreen = ({navigation}:Props) => {
  const data = [
    {
      title: i18n.t('sliderTitle1'),
      body: i18n.t('sliderBody1'),
      imgUrl: require("../../assets/images/slider1.png"), 
    },
    {
      title: i18n.t('sliderTitle2'),
      body: i18n.t('sliderBody2'),
      imgUrl: require("../../assets/images/slider2.png"),
    },
    {
      title: i18n.t('sliderTitle3'),
      body: i18n.t('sliderBody3'),
      imgUrl: require("../../assets/images/slider3.png"),
    },
  ];

  const flatListRef = useRef <FlatList<CarouselDataItem> | null>()
  const [index, setIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      const nextIndex = (index + 1) % data.length;
      flatListRef.current?.scrollToIndex({ index: nextIndex, animated: true });
      setIndex(nextIndex);
    }, 3000); // Cambia el intervalo de desplazamiento aquí (3000 ms = 3 segundos)

    return () => clearInterval(interval);
  }, [index]);
  const rendeItem =  ({item, index }: {item:CarouselDataItem, index: number}) => {
    return(
      
      <View  style={styles.container}>
        {/* <Text>{i18n.t('greet')}</Text> */}
        <View style={styles.containerImg}>
        <Image style={styles.img} source={item.imgUrl} />
        </View>
        <View style={{ flexDirection: 'row'}}>
        {data.map((_, dotIndex) => (
          <TouchableOpacity
            key={dotIndex}
            style={{ ...styles.dot,backgroundColor: dotIndex === index ? PRIMARY_COLOR : PLACEHOLDER }}
            onPress={() => {
              flatListRef.current?.scrollToIndex({ index: dotIndex, animated: true });
              setIndex(dotIndex);
            }}
          />
        ))}
        </View>
        <View style={styles.containerText}>
        <Text style={styles.textTitle}>{item.title}</Text>
        <Text style={styles.textBody}>{item.body}</Text>
        </View>
      </View>
    )
  }

  return (
    <View style={{ flex: 1,alignItems:'center'}}>
      <FlatList
        horizontal
        data={data}
        renderItem={rendeItem}
        showsHorizontalScrollIndicator={false}
        pagingEnabled
        ref={(ref)=>{flatListRef.current = ref}}
        onMomentumScrollEnd={(event) => {
          const newIndex = Math.floor(event.nativeEvent.contentOffset.x / width);
          setIndex(newIndex);
        }}
      />
      <View style={styles.btnPage}>
      <BtnPrimary 
      func={()=>{
        navigation.navigate('LoginScreen')}}
      title={i18n.t('sliderBtn')}
      />
      </View>
    </View>

  )
}

export default SliderScreen

const styles = StyleSheet.create({
  container: {
    backgroundColor: BACKGROUND,
    width,
    alignItems: 'center',
    justifyContent: 'center',
  },
  img: {
    width:'100%',
    height: 382,
    resizeMode: 'cover',
  },
  containerImg: {
    width: '90%',
    height: 382,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom:32,
  },
  containerText: {
    marginTop:15,
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle:{
    fontSize: 25,
   ...globalStyles.Title,
   color: SECONDARY_COLOR,
   textAlign: 'center',
   marginBottom:5
  },
  textBody:{
    fontSize: 18,
   ...globalStyles.Text2,
   color: SECONDARY_COLOR,
   textAlign: 'center',
   marginTop:5,
  },
  btnPage:{
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 60
  },
  dot:{
    width: 35,
    height: 8,
    borderRadius:50,
    marginHorizontal:10,
    marginBottom:15
  }
})