import { StyleSheet, Text, View } from "react-native";
import React, { useState } from "react";
import LogoContainer from "../../components/ContainerLogo/LogoContainer";
import BtnPrimary from "../../components/buttons/BtnPrimary";
import { BACKGROUND, SECONDARY_COLOR } from "../../theme/globalStyles";
import { StackScreenProps } from "@react-navigation/stack";
import i18n from "../../i18next/i18n.config";

interface Props extends StackScreenProps<any, any> {}

const SelectLanguajeScreen = ({ navigation }: Props) => {
  const [currentLanguage, setCurrentLanguage] = useState("en");
  
  const changeLanguage = (locale: string) => {
    setCurrentLanguage(locale);
    i18n.locale = locale;
  };
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <View style={styles.container}>
        <View style={styles.containerLogo}>
          <LogoContainer />
        </View>
        <View style={styles.containerBtn}>
          <BtnPrimary
            title="English"
            func={() => {
              changeLanguage("en");
              navigation.navigate("SliderScreen");
            }}
            styleBtn={styles.BtnEnglish}
          />
          <BtnPrimary
            title="Español"
            func={() => {
              navigation.navigate("SliderScreen");
              changeLanguage("es");
            }}
          />
        </View>
      </View>
    </View>
  );
};

export default SelectLanguajeScreen;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: BACKGROUND,
    alignItems: "center",
    justifyContent: "center",
  },
  containerBtn: {
    width: "80%",
    alignItems: "center",
  },
  containerLogo: {
    width: "90%",
    height: 459,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 70,
  },
  BtnEnglish: {
    backgroundColor: SECONDARY_COLOR,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
  },
});
