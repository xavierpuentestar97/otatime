import { FlatList, SafeAreaView, StyleSheet, Text, Touchable, TouchableOpacity, View } from "react-native";
import React, { useEffect, useState } from "react";
import {
  BACKGROUND,
  INPUT,
  PLACEHOLDER,
  PRIMARY_COLOR,
  SECONDARY_COLOR,
  WHITE,
  globalStyles,
} from "../../theme/globalStyles";
import { ScrollView } from "react-native-gesture-handler";
import InputText from "../../components/input/InputText";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Feather } from "@expo/vector-icons";
import CardPlace from "../../components/other/CardPlace";
import CircleBtn from "../../components/buttons/CircleBtn";
import CategoryBtn from "../../components/buttons/CategoryBtn";
import { StackScreenProps } from "@react-navigation/stack";
import { Entypo } from "@expo/vector-icons";
import { useAuthPlace } from "../../presentation/useAuthPlace";
import { getCategories } from "../../actions/category/categoryAction";
import { useQuery } from "@tanstack/react-query";
import {
  getByCategoryPlaces,
  getByCategoryPlacesId,
  getPlaces,
  getPlacesByName,
  getPopularPlaces,
} from "../../actions/place/placeAction";
import { Place } from "../../domain/entities/place";
import { BASE_URL } from '../../service/hooks/config';
import i18n from "../../i18next/i18n.config";


interface Props extends StackScreenProps<any, any> {}

const HomeScreen = ({ navigation }: Props) => {
  const [selectedBtn, setSelectedBtn] = useState("");
  const [categoryBtn, setCategoryBtn] = useState(0);
  const [searchValue, setSearchValue] = useState("");
  const [useSearch, setUseSearch] = useState(false);
  const [places, setPlaces] = useState<Place[]>([]);
  const { logOut } = useAuthPlace();
  const { user } = useAuthPlace();
  


  useEffect(() => {
    const DataSearchPlace = async () => {
      try {
        const data = searchValue
          ? await getPlacesByName(searchValue)
          : await getPlaces();
        setPlaces(data);
      } catch (error) {
        console.error("Error al obtener lugares:", error);
      }
    };
    DataSearchPlace();
  }, [searchValue]);

  const { data: categories = [] } = useQuery({
    queryKey: ["categories"],
    queryFn: () => getCategories(),
  });

  const { data: placesPopular = [] } = useQuery({
    queryKey: ["placesPopular"],
    staleTime: 5000,
    queryFn: () => getPopularPlaces(),
  });

  const { data: placesByCategory } = useQuery({
    queryKey: ["placesByCategory", categoryBtn],
    staleTime: 5000,
    queryFn: () =>
      categoryBtn ? getByCategoryPlacesId(categoryBtn) : getByCategoryPlaces(),
  });

  const onPressSearch = async () => {
    if (searchValue.length > 0) {
      setUseSearch(true);
    }
  };

  const OnpressLogOut = async () => {
    await logOut();
    navigation.navigate("LoginScreen");
  };

  const OnpressEditUser = async () => {
    await logOut();
    navigation.navigate("SignUpScreen",{
      id_user: user?.id,
    });
  };

  return (
    <View
      style={{ flex: 1, alignItems: "center", backgroundColor: "transparent" }}
    >
      <View style={styles.container}>
        <SafeAreaView>
        <ScrollView showsVerticalScrollIndicator={false} style={styles.scroll}>
          <View style={styles.useContainer}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Text style={styles.textWelcome}>{i18n.t("welcome")}</Text>
              <MaterialCommunityIcons
                name="hand-wave"
                size={12}
                color={SECONDARY_COLOR}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop:5
              }}
            >
              <TouchableOpacity style={{flexDirection: 'row'}}
              onPress={OnpressEditUser}
              >
              <Text style={styles.textUser}>{user?.user}</Text>
              <Feather name="edit" size={20} color={SECONDARY_COLOR} />
              </TouchableOpacity>

              <View
                style={{
                  alignItems: "flex-end",
                  zIndex:1,
                  position:'absolute',
                  right: 10,
                }}
              >
                <CircleBtn
                  styleBtn={styles.btnOutLogin}
                  icon={<Entypo name="log-out" size={20} color={WHITE} />}
                  func={OnpressLogOut}
                />
              </View>
            </View>
          </View>
          <View style={styles.searchContainer}>
            <Text style={styles.textTitleSearch}>{i18n.t("discover")}</Text>
            <Text style={styles.TextSearch}>{i18n.t("infoTextHome")}</Text>
            <View style={styles.containerInput}>
              <InputText
                name="search"
                value={searchValue}
                onChange={setSearchValue}
                keyboardType={"web-search"}
                styleTextInput={styles.inputSearch}
              />
              <CircleBtn
                icon={<Feather name="search" size={24} color={WHITE} />}
                styleBtn={styles.btnSearch}
                func={onPressSearch}
              />
            </View>
          </View>
          <View style={{ ...styles.containerPlace }}>
            <Text style={{ ...styles.textTitleSearch, color: SECONDARY_COLOR }}>
              {i18n.t("textAllSites")}
            </Text>
            <View style={styles.containerCardPlace}>
              <FlatList
                data={places}
                renderItem={({ item }) => (
                  <CardPlace
                    imagePlace={`${BASE_URL}${item.image}`}
                    title={item.name_place}
                    onPressCard={() => {
                      navigation.navigate("DetailsPlaceScreen", {
                        id_place: item.id_place,
                        average: item.average_rating,
                      });
                      
                    }}
                  />
                )}
                horizontal
                showsHorizontalScrollIndicator={false}
              />
            </View>
          </View>
          <View style={styles.containerPlace}>
            <Text style={{ ...styles.textTitleSearch, color: SECONDARY_COLOR }}>
              {i18n.t("textPopularSites")}
            </Text>
            <View style={styles.containerCardPlace}>
              <FlatList
                data={placesPopular}
                renderItem={({ item }) => (
                  <CardPlace
                    imagePlace={`${BASE_URL}${item.image}`}
                    title={item.name_place}
                    onPressCard={() => {
                      navigation.navigate("DetailsPlaceScreen", {
                        id_place: item.id_place,
                        average: item.average_rating,
                      });
                    }}
                  />
                )}
                horizontal
                showsHorizontalScrollIndicator={false}
              />
            </View>
          </View>
          <View style={{ ...styles.containerCategory, paddingBottom: 20 }}>
            <Text style={{ ...styles.textTitleSearch, color: SECONDARY_COLOR }}>
              {i18n.t("textCategories")}
            </Text>
            <View style={{ marginTop: 10 }}>
              <FlatList
                data={categories}
                renderItem={({ item, index }) => (
                  <CategoryBtn
                    key={index}
                    title={item.name_category}
                    imageIcon={item.image}
                    selected={selectedBtn === item.name_category}
                    func={() => {
                      setSelectedBtn(item.name_category);
                      setCategoryBtn(item.id_category);
                    }}
                    styleBtn={
                      selectedBtn === item.name_category
                        ? styles.selectedBtn
                        : undefined
                    }
                    styleTitle={
                      selectedBtn === item.name_category
                        ? styles.selectedText
                        : undefined
                    }
                  />
                )}
                horizontal
                showsHorizontalScrollIndicator={false}
              />
            </View>
            <View style={styles.containerCardPlace}>
              <FlatList
                data={placesByCategory}
                renderItem={({ item }) => (
                  <CardPlace
                    imagePlace={`${BASE_URL}${item.image}`}
                    title={item.name_place}
                    onPressCard={() => {
                      navigation.navigate("DetailsPlaceScreen", {
                        id_place: item.id_place,
                        average: item.average_rating,
                      });
                    }}
                  />
                )}
                horizontal
                showsHorizontalScrollIndicator={false}
              />
            </View>
          </View>
        </ScrollView>
        </SafeAreaView>
      </View>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent",
    width: "85%",
    // paddingBottom: 50,
  },
  scroll: {
    width: "100%",
    backgroundColor: BACKGROUND,
    marginBottom: 65,
  },
  useContainer: {
    width: "100%",
    height: 54,
    justifyContent: "center",
    marginTop: 25,
    // backgroundColor: 'pink'
  },
  textWelcome: {
    fontSize: 15,
    ...globalStyles.Text2,
    color: SECONDARY_COLOR,
    marginRight: 2,
  },
  textUser: {
    fontSize: 20,
    ...globalStyles.Text,
    color: SECONDARY_COLOR,
    marginTop: 1,
    marginRight: 2,
  },
  searchContainer: {
    width: "100%",
    marginTop: 10,
  },
  textTitleSearch: {
    fontSize: 24,
    ...globalStyles.TitleSecondary,
    color: SECONDARY_COLOR,
  },
  TextSearch: {
    fontSize: 18,
    ...globalStyles.Text,
    color: PLACEHOLDER,
    marginBottom: 20,
  },
  containerInput: {
    width: "100%",
    backgroundColor: INPUT,
    height: 62,
    flexDirection: "row",
    justifyContent: "center",
    borderRadius: 32,
  },
  inputSearch: {
    flex: 1,
    borderRadius: 32,
  },
  btnSearch: {
    alignSelf: "center",
    marginRight: 6,
  },
  containerPlace: {
    width: "100%",
    marginTop: 15,
    justifyContent: "center",
  },
  containerCardPlace: {
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },
  containerCategory: {
    width: "100%",
    marginTop: 15,
    justifyContent: "center",
  },
  selectedBtn: {
    height: 69,
    borderRadius: 16,
    backgroundColor: PRIMARY_COLOR,
    justifyContent: "center",
    alignItems: "center",
  },
  selectedText: {
    color: WHITE,
  },
  btnOutLogin: {
    width: 35,
    height: 35,
  },
});
