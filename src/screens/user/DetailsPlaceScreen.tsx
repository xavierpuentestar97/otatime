import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import React, { useEffect, useState } from "react";
import { StackScreenProps } from "@react-navigation/stack";
import {
  BACKGROUND,
  INPUT,
  SECONDARY_COLOR,
  WHITE,
  globalStyles,
} from "../../theme/globalStyles";
import HeaderScreen from "../../components/header/HeaderScreen";
import BtnSecondary from "../../components/buttons/BtnSecondary";
import MapRoutePlace from "../../components/map/MapRoutePlace";
import BtnTerciary from "../../components/buttons/BtnTerciary";
import { Octicons } from "@expo/vector-icons";
import CardComment from "../../components/other/CardComment";
import { getPlacesById } from "../../actions/place/getPlaceById";
import Scorin from "../../components/other/Scorin";
import { getCommentById } from "../../actions/comment/commetAction";
import { CommentUser } from "../../domain/entities/comment";
import { useAuthPlace } from "../../presentation/useAuthPlace";
import { BASE_URL } from "../../service/hooks/config";
import i18n from "../../i18next/i18n.config";
import { PlaceResponse } from "../../infrastructure/interfaces/places.response";

interface Props extends StackScreenProps<any, any> {}

const DetailsPlaceScreen = ({ navigation, route }: Props) => {
  const [selectedBtn, setSelectedBtn] = useState(true);
  const [comment, setComment] = useState<CommentUser[]>([]);
  const [placeById, setPlaceById] = useState<PlaceResponse[]>([]);
  const { user } = useAuthPlace();
  const id_place = route.params?.id_place;
  const user_id = user?.id;

  useEffect(() => {
    const CommentPlace = async () => {
      try {
        const commentData = await getCommentById(id_place, 1);
        setComment(commentData);
      } catch (error) {
        console.error("Error al obtener comentarios:", error);
      }
    };
    CommentPlace();
  }, [id_place]);

  useEffect(() => {
    const fetchPlaceById = async () => {
      try {
        const placeData = await getPlacesById(id_place);
        setPlaceById(placeData);
      } catch (error) {
        console.error("Error al obtener lugar por ID:", error);
      }
    };
    fetchPlaceById();
  }, [id_place]);

  const placeData = {
    name_place: placeById[0]?.name_place || "",
    address_place: placeById[0]?.address || "",
    description_place: placeById[0]?.description || "",
    description_place_en: placeById[0]?.description_en || "",
    image_place: placeById[0]?.image ? `${BASE_URL}${placeById[0].image}` : "",
    lat_place: placeById[0]?.lat,
    lng_place: placeById[0]?.lng,
    category_place: placeById[0]?.category_id || "",
    average_rating: placeById[0]?.average_rating,
  };

  function roundToOneDecimal(num: number): number {
    return Math.round(num * 10) / 10;
  }
  const roundedAverage = roundToOneDecimal(placeData.average_rating);
  
  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView style={{ backgroundColor: BACKGROUND }}>
        <ScrollView style={{ backgroundColor: BACKGROUND }}>
          <View style={styles.container}>
            <View style={styles.headerContainer}>
              <HeaderScreen
                titleHeader={placeData.name_place}
                funcBtnBack={() =>
                  navigation.navigate("TabsNavigator" as never)
                }
              />
            </View>
            <View style={styles.containerLogo}>
              <Image
                source={
                  placeData.image_place
                    ? { uri: placeData.image_place }
                    : require("../../assets/images/defaultImage.jpg")
                }
                style={styles.image}
              />
              <View style={styles.cardRatePlace}>
                <View style={styles.contentCardRate}>
                  <Text style={styles.textTitlePlace}>
                    {placeData.name_place}
                  </Text>
                  <Text style={styles.textLocation}>
                    {placeData.address_place}
                  </Text>
                  <Scorin scoreText={roundedAverage} />
                </View>
              </View>
            </View>
            <View style={styles.containerDesciption}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  width: "100%",
                }}
              >
                <View style={{ marginRight: 22 }}>
                  <Text style={styles.titleDescription}>
                    {i18n.t("textDescription")}
                  </Text>
                </View>
                <View style={{ flex: 1, marginHorizontal: 8 }}>
                  <BtnSecondary
                    title="Español"
                    enableText
                    selected={selectedBtn}
                    func={() => {
                      setSelectedBtn(!selectedBtn);
                    }}
                    styleBtn={styles.btnUnSelected}
                    styleTitle={styles.textBtnUnSelected}
                  />
                </View>
                <View style={{ flex: 1 }}>
                  <BtnSecondary
                    title="English"
                    enableText
                    selected={!selectedBtn}
                    func={() => {
                      setSelectedBtn(!selectedBtn);
                    }}
                    styleBtn={styles.btnUnSelected}
                    styleTitle={styles.textBtnUnSelected}
                  />
                </View>
              </View>
              <View style={styles.ContentDescription}>
                {selectedBtn ? (
                  <Text style={styles.textContent}>
                    {placeData.description_place}
                  </Text>
                ) : (
                  <Text style={styles.textContent}>
                    {placeData.description_place_en}
                  </Text>
                )}
              </View>
            </View>
            <View style={styles.containerMap}>
              <Text style={styles.titleDescription}>
                {i18n.t("textLocalization")}
              </Text>
              <View style={{ marginTop: 10 }}>
                <MapRoutePlace
                  lat={placeData.lat_place}
                  lng={placeData.lng_place}
                  namePlace={placeData.name_place}
                  adressPlace={placeData.address_place}
                />
              </View>
            </View>
            <View style={styles.containerComment}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  width: "100%",
                  justifyContent: "space-between",
                }}
              >
                <View style={{ marginRight: 22 }}>
                  <Text style={styles.titleDescription}>
                    {i18n.t("textComments")}
                  </Text>
                </View>
                <View style={{ width: "40%" }}>
                  <BtnTerciary
                    title={i18n.t("btnComment")}
                    enableText
                    enableIcon
                    icon={
                      <Octicons name="plus-circle" size={24} color={WHITE} />
                    }
                    styleBtn={{ justifyContent: "space-around" }}
                    func={() => {
                      navigation.navigate("CommentPlaceScreen", {
                        id_place: id_place,
                        average: placeData.average_rating,
                        id_user: user_id,
                        image_place: placeData.image_place,
                      });
                    }}
                  />
                </View>
              </View>
              <View style={styles.CardComment}>
                {comment.map((item, index) => (
                  <CardComment
                    key={index}
                    userComment={item.user_name}
                    lastName={item.user_last_name}
                    comment={item.content}
                    userCalification={item.rate}
                  />
                ))}
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default DetailsPlaceScreen;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: BACKGROUND,
    alignItems: "center",
    marginBottom: 20,
  },
  headerContainer: {
    width: "100%",
    alignItems: "center",
    marginBottom: 10,
  },
  containerLogo: {
    width: "90%",
    height: 230,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: INPUT,
    marginBottom: 10,
    borderRadius: 24,
  },
  image: {
    width: "100%",
    height: 230,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: INPUT,
    borderRadius: 24,
  },
  cardRatePlace: {
    width: "75%",
    justifyContent: "center",
    backgroundColor: INPUT,
    borderRadius: 24,
    position: "absolute",
    top: 190,
    opacity: 0.9,
  },
  contentCardRate: {
    width: "100%",
    justifyContent: "center",
    borderRadius: 24,
    marginHorizontal: 25,
    marginVertical: 8,
  },
  textTitlePlace: {
    fontSize: 22,
    ...globalStyles.TitleSecondary,
    color: SECONDARY_COLOR,
    marginBottom: 3,
  },
  textLocation: {
    fontSize: 15,
    ...globalStyles.Text2,
    color: SECONDARY_COLOR,
    marginBottom: 3,
  },
  containerDesciption: {
    width: "90%",
    marginTop: 65,
    alignItems: "center",
  },
  titleDescription: {
    fontSize: 22,
    ...globalStyles.TitleSecondary,
    color: SECONDARY_COLOR,
  },
  ContentDescription: {
    marginTop: 20,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  textContent: {
    fontSize: 18,
    ...globalStyles.Text2,
    color: SECONDARY_COLOR,
    marginBottom: 8,
    lineHeight: 28,
    textAlign: "justify",
  },
  btnUnSelected: {
    width: "100%",
    height: 40,
    borderRadius: 24,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: WHITE,
  },
  textBtnUnSelected: {
    fontSize: 16,
    color: SECONDARY_COLOR,
    ...globalStyles.TitleSecondary,
    marginHorizontal: 10,
  },
  containerMap: {
    width: "90%",
    justifyContent: "center",
    marginTop: 10,
  },
  containerComment: {
    width: "90%",
    marginTop: 20,
  },
  CardComment: {
    width: "100%",
    marginTop: 8,
  },
});
