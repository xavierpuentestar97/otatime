import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import React, { useState } from "react";
import { StackScreenProps } from "@react-navigation/stack";
import {
  BACKGROUND,
  INPUT,
  PRIMARY_COLOR,
  SECONDARY_COLOR,
  globalStyles,
} from "../../theme/globalStyles";
import HeaderScreen from "../../components/header/HeaderScreen";
import InputTextArea from "../../components/input/InputTextArea";
import StarsCalifications from "../../components/other/StarsCalifications";
import BtnPrimary from "../../components/buttons/BtnPrimary";
import { useCreateComment } from "../../presentation/useCreateComment";
import { getCommentById } from "../../actions/comment/commetAction";
import { CommentUser } from "../../domain/entities/comment";
import i18n from "../../i18next/i18n.config";

interface Props extends StackScreenProps<any, any> {}

const CommentPlaceScreen = ({ navigation, route }: Props) => {
  const id_place = route.params?.id_place;
  const average = route.params?.average;
  const user_id = route.params?.id_user;
  const image_place = route.params?.image_place;
  const { menssageComment } = useCreateComment();
  const [comment, setComment] = useState<CommentUser[]>([]);

  const [form, setForm] = useState({
    content: "",
    user_id: user_id,
    rate: 0,
    date: new Date(),
    place_id: id_place,
  });

  const onCreateComment = async () => {
    if (
      form.content.length === 0 ||
      form.user_id === 0 ||
      form.rate === 0 ||
      form.date === undefined ||
      form.place_id === 0
    ) {
      return;
    }
    const wasSuccessFull = await menssageComment(
      form.content,
      form.user_id,
      form.rate,
      form.date,
      form.place_id
    );
    const commentData = await getCommentById(id_place, 1);
        setComment(commentData);
    if (wasSuccessFull) {
      navigation.replace("DetailsPlaceScreen", {
        id_place: form.place_id,
        average: average,
      });
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView style={{ backgroundColor: PRIMARY_COLOR }}>
        <ScrollView style={{ backgroundColor: BACKGROUND }}>
          <View style={styles.container}>
            <View style={styles.headerContainer}>
              <HeaderScreen
                titleHeader={i18n.t("textTitleComment")}
                funcBtnBack={() =>
                  navigation.navigate("DetailsPlaceScreen", {
                    id_place: id_place,
                    average: average,
                  })
                }
              />
            </View>
            <View style={styles.containerLogo}>
              <Image source={{ uri: image_place }} style={styles.img} />
            </View>

            <View style={styles.containerIput}>
              <View style={{ marginBottom: 25 }}>
                <Text style={styles.text}>
                {i18n.t("textSubtitleComment")}
                </Text>
                <Text style={styles.text}>{i18n.t("textContent")}</Text>
              </View>
              <View
                style={{ height: 30, alignItems: "center", marginBottom: 25 }}
              >
                <StarsCalifications
                  starCalification={(value: number) => {
                    setForm({ ...form, rate: value });
                  }}
                />
              </View>
              <InputTextArea
                name="content"
                value={form.content}
                onChange={(content: string) => setForm({ ...form, content })}
                text={i18n.t("inputComment")}
              />
            </View>
            <View style={styles.containerBtn}>
              <BtnPrimary title={i18n.t("btnSend")} func={() => onCreateComment()} />
              <BtnPrimary
                title={i18n.t("btnNotSend")}
                func={() => {
                  navigation.navigate("DetailsPlaceScreen", {
                    id_place: id_place,
                    average: average,
                  });
                }}
                styleBtn={{ backgroundColor: BACKGROUND }}
                styleTitle={styles.textBtnInvitado}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default CommentPlaceScreen;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: BACKGROUND,
    alignItems: "center",
  },
  headerContainer: {
    width: "100%",
    height: 210,
    alignItems: "center",
    backgroundColor: PRIMARY_COLOR,
    borderBottomRightRadius: 24,
    borderBottomLeftRadius: 24,
  },
  containerLogo: {
    width: 230,
    height: 230,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: INPUT,
    borderRadius: 150,
    position: "absolute",
    marginTop: 80,
  },
  img: {
    width: 215,
    height: 215,
    borderRadius: 150,
  },
  containerIput: {
    width: "80%",
    alignItems: "center",
    marginTop: 124,
  },
  text: {
    fontSize: 17,
    ...globalStyles.Text2,
    color: SECONDARY_COLOR,
    textAlign: "center",
    marginBottom: 10,
  },
  containerBtn: {
    width: "80%",
    alignItems: "center",
    marginTop: 70,
    alignSelf: "center",
  },
  textBtnInvitado: {
    color: SECONDARY_COLOR,
  },
});
