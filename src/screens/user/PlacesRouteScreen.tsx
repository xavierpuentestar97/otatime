import {
  FlatList,
  StyleSheet,
  View,
} from "react-native";
import React, { useEffect, useRef, useState } from "react";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import * as Location from "expo-location";
import CircleBtn from "../../components/buttons/CircleBtn";
import {
  BLACK,
  CARDHOME,
  PRIMARY_COLOR,
  WHITE,
} from "../../theme/globalStyles";
import { Entypo } from "@expo/vector-icons";
import { Feather } from "@expo/vector-icons";
import MapViewDirections from "react-native-maps-directions";
import FAB from "../../components/buttons/FAB";
import { useQuery } from "@tanstack/react-query";
import {
  getByCategoryPlaces,
  getByCategoryPlacesId,
} from "../../actions/place/placeAction";
import { getCategories } from "../../actions/category/categoryAction";
import CategoryMapBtn from "../../components/buttons/CategoryMapBtn";

const PlacesRouteScreen = () => {
  const [location, setLocation] = useState({ latitude: 0, longitude: 0 });
  const [destination, setDestination] = useState({ latitude: 0, longitude: 0 });
  const [categoryBtn, setCategoryBtn] = useState(0);
  const [selectedBtn, setSelectedBtn] = useState("");
  const mapRef = useRef<MapView>();
  useEffect(() => {
    getPermitionLocation();
    getUserLocation();
  }, []);

  const getPermitionLocation = async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      return;
    }
  };

  const getUserLocation = async () => {
    let location = await Location.getCurrentPositionAsync({});
    setLocation(location.coords);
  };

  //datos por categoria
  const { data: placesByCategory = [] } = useQuery({
    queryKey: ["placesByCategory", categoryBtn],
    staleTime: 5000,
    queryFn: () =>
      categoryBtn ? getByCategoryPlacesId(categoryBtn) : getByCategoryPlaces(),
  });
  const { data: categories = [] } = useQuery({
    queryKey: ["categories"],
    queryFn: () => getCategories(),
  });

  const handlePress = (latitude: number, longitude: number) => {
    setDestination({ latitude, longitude });
  };

  const GOOGLE_API_KEY = "AIzaSyBQ1GqVWCmfaDf-pZyA5YbrcFqKA1WEg9w";

  const navigationCamera = async () => {
    if (!mapRef.current) return;
    mapRef.current?.animateCamera({
      center: location,
      zoom: 14.5,
    });
  };
  return (
    <View style={styles.container}>
      <View
        style={{
          borderRadius: 24,
          overflow: "hidden",
          width: "90%",
          height: "90%",
        }}
      >
        <MapView
          ref={(map) => (mapRef.current = map!)}
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          region={{
            latitude: location.latitude,
            longitude: location.longitude,
            latitudeDelta: 0.9,
            longitudeDelta: 0.9,
          }}
        >
          {destination.latitude !== 0 &&
          destination.longitude !== 0 &&
          location.latitude !== 0 &&
          location.longitude !== 0 ? (
            <MapViewDirections
              origin={{
                latitude: location.latitude,
                longitude: location.longitude,
              }}
              destination={destination}
              apikey={GOOGLE_API_KEY}
              strokeWidth={3}
              strokeColor={PRIMARY_COLOR}
            />
          ) : undefined}

          <Marker
            coordinate={{
              latitude: location.latitude,
              longitude: location.longitude,
            }}
            title="Mi Ubicacion"
            description="Description"
          >
            <CircleBtn
              icon={<Feather name="map-pin" size={24} color={WHITE} />}
              styleBtn={{ backgroundColor: CARDHOME }}
            />
          </Marker>

          {placesByCategory.map((item, index) => (
            <Marker
              coordinate={{
                latitude: item.lat,
                longitude: item.lng,
              }}
              title={item.name_place}
              description={item.address}
              key={index}
              onPress={() => handlePress(item.lat, item.lng)}
            >
              <CircleBtn
                icon={<Entypo name="shop" size={24} color={WHITE} />}
                styleBtn={styles.markerBtn}
                func={() => console.log("prueba")}
              />
            </Marker>
          ))}
        </MapView>
        <FAB
          icon={<Feather name="compass" size={24} color={WHITE} />}
          onPress={() => navigationCamera()}
          styleBtn={{ bottom: 20, right: 20 }}
        />
        <View style={{ position: "absolute", top: 15, right: 0 }}>
          <FlatList
            data={categories}
            renderItem={({ item, index }) => (
              <CategoryMapBtn
                key={index}
                title={item.name_category}
                imageIcon={item.image}
                selected={selectedBtn === item.name_category}
                func={() => {
                  setSelectedBtn(item.name_category);
                  setCategoryBtn(item.id_category);
                }}
                styleBtn={
                  selectedBtn === item.name_category
                    ? styles.selectedBtn
                    : undefined
                }
                styleTitle={
                  selectedBtn === item.name_category
                    ? styles.selectedText
                    : undefined
                }
              />
            )}
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    </View>
  );
};

export default PlacesRouteScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  map: {
    width: "100%",
    height: "100%",
  },
  markerBtn: {
    backgroundColor: PRIMARY_COLOR,
  },
  selectedBtn: {
    height: 40,
    backgroundColor: BLACK,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 5,
    opacity: 0.9,
    elevation: 5,
    borderTopLeftRadius: 16,
    borderBottomLeftRadius: 16,
  },
  selectedText: {
    color: WHITE,
  },
});
