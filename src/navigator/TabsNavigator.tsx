import { StyleSheet, Text, View } from "react-native";
import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Octicons } from "@expo/vector-icons";
import {
  BACKGROUND,
  PRIMARY_COLOR,
  SECONDARY_COLOR,
  WHITE,
  globalStyles
} from "../theme/globalStyles";
import { FontAwesome5 } from "@expo/vector-icons";
import HomeScreen from "../screens/shared/HomeScreen";
import PlacesRouteScreen from "../screens/user/PlacesRouteScreen";
import i18n from "../i18next/i18n.config";
const Tab = createBottomTabNavigator();


const TabsNavigator = () => {
  return (
    <Tab.Navigator
      sceneContainerStyle={{
        backgroundColor: BACKGROUND,
      }}
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {
          backgroundColor: BACKGROUND,
          position: "absolute",
          borderTopColor: BACKGROUND,
          height:75
        },
        tabBarHideOnKeyboard: true,
        tabBarActiveTintColor: WHITE,
        tabBarInactiveTintColor: SECONDARY_COLOR,
      }}
    >
      <Tab.Screen
        name="Inicio"
        component={HomeScreen}
        options={({ navigation }) => ({
          tabBarIcon: ({ color }) => (
            <View style={{...styles.container, backgroundColor: navigation.isFocused() ? PRIMARY_COLOR : WHITE}}>
              <Octicons name="home" size={24} color={color} />
              <Text style={{ ...styles.text , color: color}}>{i18n.t("btnHome")}</Text>
            </View>
          ),
        })}
      />

      <Tab.Screen
        name="Rutas"
        component={PlacesRouteScreen}
        options={({ navigation }) => ({
            tabBarIcon: ({ color }) => (
                <View style={{...styles.container, backgroundColor: navigation.isFocused() ? PRIMARY_COLOR : WHITE}}>
               <FontAwesome5 name="map-marked-alt" size={22} color={color} />
                <Text style={{...styles.text , color: color }}>{i18n.t("btnRoutes")}</Text>
              </View>
            ),
        })}
      />
    </Tab.Navigator>
  );
};

export default TabsNavigator;

const styles = StyleSheet.create({
    container:{
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        width:90,
        height:40,
        borderRadius:16,
    },
    text:{
        marginLeft: 5,
        ...globalStyles.TitleSecondary,
        fontSize: 15,
    }
})
