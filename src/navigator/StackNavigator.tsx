import {
  StackCardStyleInterpolator,
  createStackNavigator,
} from "@react-navigation/stack";
import HomeScreen from "../screens/shared/HomeScreen";
import { BACKGROUND } from "../theme/globalStyles";
import TabsNavigator from "./TabsNavigator";
import SelectLanguajeScreen from "../screens/shared/SelectLanguajeScreen";
import SliderScreen from "../screens/shared/SliderScreen";
import LoginScreen from "../screens/auth/LoginScreen";
import SignUpScreen from "../screens/auth/SignUpScreen";
import DetailsPlace from "../screens/user/DetailsPlaceScreen";
import CommentPlaceScreen from "../screens/user/CommentPlaceScreen";
import DetailsPlaceScreen from "../screens/user/DetailsPlaceScreen";
import HomeAdminSreen from "../screens/admin/HomeAdminScreen";
import AddPlaceScreen from "../screens/admin/AddPlaceScreen";
import UserStateScreen from "../screens/admin/UserStateScreen";
import ListPlacesScreen from "../screens/admin/ListPlacesScreen";
import PlacesCommentScreen from "../screens/admin/PlacesCommentScreen";
import CommentsPlaceAllScreen from "../screens/admin/CommentsPlaceAllScreen";
import { useAuthPlace } from "../presentation/useAuthPlace";
import ForgotPassScreen from "../screens/auth/ForgotPassScreen";

export type RootStackParams = {
  LoginScreen: undefined;
  SignUpScreen: undefined;
  HomeScreen: undefined;
  DetailsPlaceScreen: { placeId: number };
};

const Stack = createStackNavigator();
const fadeAnimation: StackCardStyleInterpolator = ({ current }) => {
  return {
    cardStyle: {
      opacity: current.progress,
    },
  };
};

export const StackNavigator = () => {
  const { checkStatus, status } = useAuthPlace();
  const { user } = useAuthPlace();
  return (
    <Stack.Navigator
      initialRouteName="SelectLanguajeScreen"
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: BACKGROUND,
        },
        // cardStyleInterpolator: fadeAnimation
      }}
    >
      {status !== "authenticated" ? (
        <>
          <Stack.Screen
            name="SelectLanguajeScreen"
            component={SelectLanguajeScreen}
          />
          <Stack.Screen name="SliderScreen" component={SliderScreen} />
          <Stack.Screen
            options={{ cardStyleInterpolator: fadeAnimation }}
            name="LoginScreen"
            component={LoginScreen}
          />
          <Stack.Screen
            options={{ cardStyleInterpolator: fadeAnimation }}
            name="SignUpScreen"
            component={SignUpScreen}
          />
          <Stack.Screen
            options={{ cardStyleInterpolator: fadeAnimation }}
            name="ForgotPassScreen"
            component={ForgotPassScreen}
          />
        </>
      ) : (
        <>
          {user?.usertype_id === 2 ? (
            <>
              <Stack.Screen name="TabsNavigator" component={TabsNavigator} />
              <Stack.Screen name="HomeScreen" component={HomeScreen} />
              <Stack.Screen
                name="DetailsPlaceScreen"
                component={DetailsPlaceScreen}
              />
              <Stack.Screen
                name="CommentPlaceScreen"
                component={CommentPlaceScreen}
              />
            </>
          ) : (
            <>
              <Stack.Screen name="HomeAdminScreen" component={HomeAdminSreen} />
              <Stack.Screen name="AddPlaceScreen" component={AddPlaceScreen} />
              <Stack.Screen
                name="UserStateScreen"
                component={UserStateScreen}
              />
              <Stack.Screen
                name="ListPlacesScreen"
                component={ListPlacesScreen}
              />
              <Stack.Screen
                name="PlacesCommentScreen"
                component={PlacesCommentScreen}
              />
              <Stack.Screen
                name="CommentsPlaceAllScreen"
                component={CommentsPlaceAllScreen}
              />
            </>
          )}
        </>
      )}
    </Stack.Navigator>
  );
};
