import { StyleSheet, Text, View, ViewStyle } from 'react-native'
import React from 'react'
import { INPUT, SECONDARY_COLOR, globalStyles } from '../../theme/globalStyles'
import BackBtn from '../buttons/BackBtn'
import LogoContainer from '../ContainerLogo/LogoContainer'

interface Props {
  funcBtnBack?: () => void
  iconHeader?: boolean
  titleHeader?: string
}

const HeaderScreen = ({iconHeader, titleHeader, funcBtnBack}:Props) => {
  return (
    <View style={styles.container}>
      <BackBtn func={funcBtnBack}/>
      <View style={styles.containerText}>
      <Text style={styles.text}>{titleHeader}</Text>
      </View>
      <View style={{...styles.iconHeader}}>
        {iconHeader?
        <LogoContainer styleImg={styles.img} styleContainer={styles.containerImg}/>
        :<View/> }
      </View>
    </View>
  )
}

export default HeaderScreen

const styles = StyleSheet.create({
  container: {
    width:'100%',
    height:60,
    borderRadius: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'center',
    marginTop:10,
  },
  containerText:{
    flex:1,
    alignItems: 'center',
  },
  text:{
    fontSize: 24,
    ...globalStyles.TitleSecondary,
    color: SECONDARY_COLOR
  },
  iconHeader:{
    width:50,
    height:50,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight:10,
  },
  img:{
    width:25,
    height:35,
    borderRadius: 25,
    justifyContent: 'center',
  },
  containerImg:{
    width:50,
    height:50,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: INPUT
  }
})