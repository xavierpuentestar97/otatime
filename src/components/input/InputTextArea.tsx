import { KeyboardTypeOptions, StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native'
import React from 'react'
import { TextInput } from 'react-native-gesture-handler';
import { INPUT, SECONDARY_COLOR, globalStyles } from '../../theme/globalStyles';

interface Props {
  text?: string;
  name?: string;
  onChange: any;
  keyboardType?: KeyboardTypeOptions;
  style?: StyleProp<ViewStyle>;
  secureTextEntry?: boolean;
  prefixIconName?: any;
  autoCapitalize?: any;
  onSubmitEditing?: () => void;
  value?: string;
  multiline?: any;
  styleInput?: any;
  autofocus?: boolean;
}

type prefixIconName = "user" | "lock" | "key" | "mail" | "grid";
type autoCapitalize = "none" | "sentences" | "words" | "characters";


const InputTextArea = ({
  text,
  onChange,
  name,
  style,
  autoCapitalize,
  onSubmitEditing,
  keyboardType,
  value,
  multiline,
  autofocus,
  styleInput,
}: Props) => {
  return (
    <View style={{ ...styles.container, ...(style as any) }}>
      <View style={{ ...styles.containerInput }}>
        <TextInput
          style={{
            ...styles.inpuStyle,
            ...styleInput
          }}
          placeholder={text}
          value={value}
          onChangeText={(value) => onChange(value, name)}
          autoCapitalize={autoCapitalize}
          multiline={multiline}
          numberOfLines={20}
          onSubmitEditing={onSubmitEditing}
          keyboardType={keyboardType}
          autoFocus={autofocus}

        />
      </View>
    </View>
  )
}

export default InputTextArea

const styles = StyleSheet.create({
  container: {
    height: 150,
    backgroundColor: INPUT,
    borderRadius: 16,
  },
  containerInput: {
    flex: 1,
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
  },
  inpuStyle: {
    width: "100%",
    borderRadius: 10,
    marginVertical: 10,
    ...globalStyles.Text2,
    color: SECONDARY_COLOR,
    fontSize: 18,
    textAlign:"center"
  },

})