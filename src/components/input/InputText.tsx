import { KeyboardTypeOptions, NativeSyntheticEvent, StyleSheet, TextInputKeyPressEventData, TextStyle, TouchableOpacity, View, ViewStyle } from 'react-native'
import React, { useState } from 'react'
import { TextInput } from 'react-native-gesture-handler'
import { INPUT, PLACEHOLDER, SECONDARY_COLOR, globalStyles } from '../../theme/globalStyles'
import { Feather } from '@expo/vector-icons';

interface props {
    text?: string;
    name?: string;
    placeholder?: any;
    value?: any;
    onChange?: any;
    keyboardType?: KeyboardTypeOptions;
    styleText?: ViewStyle;
    secureTextEntry?: boolean;
    editable?: any;
    icon?: JSX.Element,
    onKeyPress?: (e: NativeSyntheticEvent<TextInputKeyPressEventData>) => void;
    maxLength?: any
    styleTextInput?: TextStyle
}

const InputText = (
    {
        icon,
        editable,
        text,
        onChange,
        name,
        value,
        styleText,
        keyboardType,
        secureTextEntry,
        onKeyPress,
        placeholder,
        maxLength,
        styleTextInput,

    }: props) => {

    const [visiblePassword, setVisiblePassword] = useState(true);

    return (
        <View style={{...styles.container, ...styleTextInput}}>
            <View style={styles.icon}>
                {icon}
            </View>
            <TextInput
                editable={editable === undefined || editable === null
                    ? true : editable === true
                        ? true : editable === false
                            ? false : false
                }
                style={styles.inputText}
                placeholderTextColor={PLACEHOLDER}
                placeholder={text}
                value={value}
                onChangeText={(value) => onChange(value, name)}
                autoCapitalize='none'
                keyboardType={keyboardType}
                onKeyPress={(e) => (onKeyPress ? onKeyPress(e) : () => { })}
                secureTextEntry={secureTextEntry ? visiblePassword : false}
                maxLength={maxLength}
            />
            <TouchableOpacity
                onPress={() => setVisiblePassword(!visiblePassword)}
                style={styles.showPass}
            >
                {secureTextEntry && (
                    <Feather
                        name={visiblePassword ? 'eye' : 'eye-off'}
                        size={24}
                        color={PLACEHOLDER}
                    />
                )}

            </TouchableOpacity>
        </View>
    )
}

export default InputText

const styles = StyleSheet.create({
    inputText: {
        flex: 1,
        height: 62,
        borderRadius: 16,
        paddingRight: 14,
        ...globalStyles.Text2,
        color: SECONDARY_COLOR,
        fontSize: 18
    },
    container: {
        width: '100%',
        height: 62,
        borderRadius: 16,
        backgroundColor: INPUT,
        marginBottom: 15,
        flexDirection: 'row',
    },
    icon: {
        width: 65,
        height: 62,
        justifyContent: 'center',
        alignItems: 'center',
    },
    showPass: {
        width: 40,
        height: 62,
        left: -15,
        justifyContent: 'center',
        alignItems: 'center',
    }
})