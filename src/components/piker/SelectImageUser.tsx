import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native";
import React, { useState } from "react";
import {
  INPUT,
  PLACEHOLDER,
  WHITE,
  globalStyles,
} from "../../theme/globalStyles";
import { MaterialIcons } from "@expo/vector-icons";
import FAB from "../buttons/FAB";
import { FontAwesome } from "@expo/vector-icons";

interface Props {
  pickImage?: any;
  uri?: any;
  imageUrl?: any;
  styleImg?: ViewStyle;
}

const SelectImageUser = ({ pickImage, uri, styleImg, imageUrl }: Props) => {
  const [file, setFile] = useState({
    name: "",
    uri: "",
  });

  const selectImage = async () => {
    let result = await pickImage();
    if (result !== null) {
      setFile({
        name: result.fileName,
        uri: result.path,
      });
      uri(
        JSON.parse(
          JSON.stringify({
            uri: result.path,
            type: `image/${result.path.split(".").pop()}`,
            name: result.path.split("/").pop(),
          })
        )
      );
    }
  };
  const removeImg = () => {
    Alert.alert("Confirmación", "¿Quieres remover esta imagen?", [
      {
        text: "Cancelar",
        style: "cancel",
      },
      {
        text: "Sí",
        onPress: () => {
          setFile({
            name: "",
            uri: "",
          });
        },
      },
    ]);
  };

  return (
    <View style={{ alignItems: "center" }}>
      <View style={styles.containerText}>
        <Text style={styles.text}>Agregar Imagen</Text>
      </View>
      <TouchableOpacity style={[styles.button, styleImg]} onPress={selectImage}>
        {file.uri === "" && !imageUrl ? (
          <MaterialIcons name="photo-library" size={50} color={PLACEHOLDER} />
        ) : (
          <Image
            source={{ uri: file.uri === "" ? imageUrl : file.uri }}
            style={styles.image}
            resizeMode="cover"
          />
        )}
      </TouchableOpacity>
      <FAB
        icon={<FontAwesome name="remove" size={24} color={WHITE} />}
        onPress={removeImg}
        styleBtn={{ right: 5, top: 40 }}
      />
    </View>
  );
};

export default SelectImageUser;

const styles = StyleSheet.create({
  button: {
    width: "100%",
    height: 180,
    borderRadius: 24,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: INPUT,
  },
  image: {
    width: "100%",
    height: 180,
    borderRadius: 24,
  },
  containerText: {
    width: "95%",
    justifyContent: "center",
    marginBottom: 8,
  },
  text: {
    fontSize: 20,
    ...globalStyles.Text2,
    color: PLACEHOLDER,
  },
});
