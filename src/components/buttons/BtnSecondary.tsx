import { StyleSheet, Text, TextStyle, TouchableOpacity, View, ViewStyle } from 'react-native'
import React from 'react'
import { PRIMARY_COLOR, WHITE, globalStyles } from '../../theme/globalStyles'


interface Props {
    title: string
    func?: () => void
    styleBtn?: ViewStyle
    styleTitle?: TextStyle
    icon?: JSX.Element
    enableIcon?: boolean
    enableText?: boolean
    selected?: boolean; 
}


const BtnSecondary = ({ title, func, styleBtn, styleTitle, icon, enableIcon, enableText, selected }: Props) => {
    return (
        <TouchableOpacity style={{ ...(selected ? styles.btnContainer : styleBtn) }} onPress={func}>
            {enableText ?
                <Text style={{ ...(selected ? styles.textBtn : styleTitle) }}>{title}</Text>
                : <View />
            }
            {enableIcon ? icon : <View />}
        </TouchableOpacity>
    )
}

export default BtnSecondary

const styles = StyleSheet.create({
    btnContainer: {
        width: '100%',
        height: 40,
        borderRadius: 24,
        backgroundColor: PRIMARY_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textBtn: {
        fontSize: 16,
        color: WHITE,
        ...globalStyles.TitleSecondary,
        marginHorizontal: 10,
    }
})