import { StyleSheet, TouchableOpacity, ViewStyle } from 'react-native'
import React from 'react'
import { SECONDARY_COLOR } from '../../theme/globalStyles'

interface Props {
    func?: () => void
    icon?: JSX.Element
    styleBtn?: ViewStyle
  }


const CircleBtn = ({func,icon,styleBtn}:Props) => {
  return (
    <TouchableOpacity style={{...styles.searchBtn,...styleBtn}} onPress={func}>
      {icon}
    </TouchableOpacity>
  )
}

export default CircleBtn

const styles = StyleSheet.create({
    searchBtn:{
        width:45,
        height:45,
        borderRadius:24,
        backgroundColor:SECONDARY_COLOR,
        justifyContent:'center',
        alignItems:'center'
    },
    searchIcon:{
        width:29,
        height:29,
    }
})