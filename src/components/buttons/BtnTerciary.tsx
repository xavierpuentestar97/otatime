import { StyleSheet, Text, TextStyle, TouchableOpacity, View, ViewStyle } from 'react-native'
import React from 'react'
import { PRIMARY_COLOR, WHITE, globalStyles } from '../../theme/globalStyles'

interface Props {
    title?: string
    func?: () => void
    styleBtn?: ViewStyle
    styleTitle?: TextStyle
    icon?: JSX.Element
    enableIcon?: boolean
    enableText?: boolean
}


const BtnTerciary = ({ title, func, styleBtn, styleTitle, icon, enableIcon, enableText }: Props) => {
  return (
    <TouchableOpacity style={{ ...styles.btnContainer, ...styleBtn }} onPress={func}>
            {enableText ?
                <Text style={{ ...styles.textBtn, ...styleTitle }}>{title}</Text>
                : <View />
            }
            {enableIcon ? icon : <View />}
        </TouchableOpacity>
  )
}

export default BtnTerciary

const styles = StyleSheet.create({
    btnContainer: {
        width: '100%',
        height: 40,
        borderRadius: 24,
        backgroundColor: PRIMARY_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textBtn: {
        fontSize: 16,
        color: WHITE,
        ...globalStyles.TitleSecondary,
        marginHorizontal: 10,
    }
})