import { Image, StyleSheet, Text, TextStyle, TouchableOpacity, ViewStyle } from 'react-native'
import React from 'react'
import { BACKGROUND, PRIMARY_COLOR,SECONDARY_COLOR,WHITE,globalStyles } from '../../theme/globalStyles'

interface Props {
    func?: () => void
    title: string
    styleBtn?: ViewStyle
    styleTitle?: TextStyle
    imageIcon?: string
    selected?: boolean; 

  }

const CategoryBtn = ({func,title,styleBtn,styleTitle,imageIcon,selected}:Props) => {
  return (
    <TouchableOpacity style={{ ...(selected ? styleBtn: styles.categoryBtn) }} onPress={func}>
        <Image source={{uri:imageIcon}} style={styles.searchIcon}/>
      <Text style={{...styles.textBtn,...styleTitle}}>{title}</Text>
    </TouchableOpacity>
  )
}

export default CategoryBtn

const styles = StyleSheet.create({
    categoryBtn:{
        // width:68,
        height:69,
        borderRadius:16,
        backgroundColor:BACKGROUND,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal:5
        
    },
    searchIcon:{
        width:40,
        height:40,
       marginHorizontal:10
    },
    textBtn:{
        fontSize: 12,
       ...globalStyles.TitleSecondary,
       marginHorizontal:10,
       color: SECONDARY_COLOR
    }
})