import { StyleSheet, Text, TextStyle, TouchableOpacity, View, ViewStyle } from 'react-native'
import React from 'react'
import { BACKGROUND, SECONDARY_COLOR, globalStyles } from '../../theme/globalStyles'
interface Props {
    func?: () => void
    title: string
    styleBtn?: ViewStyle
    styleTitle?: TextStyle
    imageIcon?: string
    selected?: boolean; 

  }
const CategoryMapBtn = ({func,title,styleBtn,styleTitle,imageIcon,selected}:Props) => {
  return (
    <TouchableOpacity style={{ ...(selected ? styleBtn: styles.categoryBtn) }} onPress={func}>
      <Text style={{...styles.textBtn,...styleTitle}}>{title}</Text>
    </TouchableOpacity>
  )
}

export default CategoryMapBtn

const styles = StyleSheet.create({
    categoryBtn:{
        // width:68,
        height:40,
        // borderRadius:16,
        backgroundColor:BACKGROUND,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical:5,
        opacity: 0.9,
        elevation: 5,
        borderTopLeftRadius:16,
        borderBottomLeftRadius:16,
    },
    textBtn:{
        fontSize: 12,
       ...globalStyles.TitleSecondary,
       marginHorizontal:10,
       color: SECONDARY_COLOR
    }
})