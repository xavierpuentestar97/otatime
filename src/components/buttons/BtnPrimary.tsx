import { StyleSheet, Text, TextStyle, TouchableOpacity, ViewStyle } from "react-native"
import React from "react";
import { PRIMARY_COLOR,WHITE,globalStyles } from "../../theme/globalStyles";

interface Props {
  title: string;
  func: () => void;
  styleBtn?: ViewStyle;
  styleTitle?: TextStyle;
  disableBtn?: boolean;
}

const BtnPrimary = ({ title, func, styleBtn, styleTitle, disableBtn }: Props) => {
  const handlePressBtn = () => {
    func()}

  return (
    <TouchableOpacity onPress={handlePressBtn} style={{...styles.btnPrimary,...styleBtn}} disabled={disableBtn}>
      <Text style={{...styles.btnText, ...styleTitle}}>{title}</Text>
    </TouchableOpacity>
  );
};

export default BtnPrimary

const styles = StyleSheet.create({
    btnPrimary: {
      width: '100%',
      height: 54,
      borderRadius: 8,
      backgroundColor: PRIMARY_COLOR,
      justifyContent: "center",
      alignItems: "center",
      marginBottom: 20,
    },
    btnText: {
      fontSize: 23,
      ...globalStyles.TitleSecondary,
      color: WHITE
    },
})
