import { Pressable, StyleSheet, Text, View, ViewStyle } from "react-native";
import React from "react";

interface Props {
  icon?: JSX.Element;
  onPress?: () => void;
  styleBtn?: ViewStyle;
}

const FAB = ({ icon, onPress, styleBtn }: Props) => {
  return (
    <View style={{ ...styles.btn, ...styleBtn }}>
      <Pressable onPress={onPress}>{icon}</Pressable>
    </View>
  );
};

export default FAB;

const styles = StyleSheet.create({
  btn: {
    zIndex: 1,
    position: "absolute",
    height: 50,
    width: 50,
    borderRadius: 30,
    backgroundColor: "black",
    justifyContent: "center",
    alignItems: "center",
    elevation: 5,
    opacity: 0.9
  },
});
