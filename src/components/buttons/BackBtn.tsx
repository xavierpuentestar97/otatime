import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { SECONDARY_COLOR } from '../../theme/globalStyles'
import { Ionicons } from '@expo/vector-icons';

interface Props {
    func?: () => void
  }

const BackBtn = ({func}:Props) => {
  return (
    < TouchableOpacity style={styles.btnBack} onPress={func}>
      <Ionicons name="chevron-back" size={60} color={SECONDARY_COLOR}/>
    </TouchableOpacity>
  )
}

export default BackBtn

const styles = StyleSheet.create({
    btnBack:{
        width: 50,
        height: 60,
        borderRadius: 16,
    },
})