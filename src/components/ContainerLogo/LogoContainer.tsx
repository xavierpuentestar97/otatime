import { Dimensions, Image, ImageBackground, StyleSheet, Text, View, ViewStyle } from 'react-native'
import React from 'react'
import { BACKGROUND } from '../../theme/globalStyles';

interface Props {
    styleContainer?: ViewStyle
    styleImg?: ViewStyle
}
const { width: screenWidth, height: screenHeight } = Dimensions.get("window");
const LogoContainer = ({ styleContainer, styleImg }: Props) => {
    return (
        <View style={{ ...styles.containerImg, ...styleContainer }}>
            <ImageBackground
                source={require('../../assets/icons/logoOtaTime.png')}
                style={{ ...styles.image, ...styleImg }}
                resizeMode="cover"
            />
        </View>
    )
}

export default LogoContainer

const styles = StyleSheet.create({
    containerImg: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 25,
        width: 325,
        height: 459,
    },
    image: {
        width: 325,
        height: 459,
    }
})