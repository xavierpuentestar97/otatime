import { StyleSheet, Text, View } from "react-native";
import React, { useEffect, useRef, useState } from "react";
import MapView, { Marker, PROVIDER_GOOGLE, Camera } from "react-native-maps";
import * as Location from "expo-location";
import CircleBtn from "../buttons/CircleBtn";
import { CARDHOME, PRIMARY_COLOR, WHITE } from "../../theme/globalStyles";
import { Feather } from "@expo/vector-icons";
import FAB from "../buttons/FAB";



interface Props {
    onMarkerPress?: (latitude: number, longitude: number, adress:string) => void;
    lat?: number;
    lng?: number;
  }
  
  interface LocationData {
    city?: string;
    street?: string;
    name?: string;
    postalCode?: string;
    region?: string;
    country?: string;
}

const MapCreatePlace = ({onMarkerPress, lat, lng}:Props) => {
  const [location, setLocation] = useState({ latitude: 0, longitude: 0 });
  const [markerLocation, setMarkerLocation] = useState<{ latitude: number; longitude: number } | null>(null);
  
  const mapRef = useRef< MapView >()
  useEffect(() => {
    navigationCamera();
    getPermitionLocation();
    getUserLocation();
  }, []);

  useEffect(() => {
    if (lat && lng) {
      setLocation({ latitude: lat, longitude: lng });
      setMarkerLocation({ latitude: lat, longitude: lng });
    }
    navigationCamera();

  }, [lat])
  

  const getPermitionLocation = async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      return;
    }
  };

  const getUserLocation = async () => {
    let location = await Location.getCurrentPositionAsync({});
    setLocation(location.coords);
  };

  const navigationCamera = async() =>{
    if(!mapRef.current) return;
    mapRef.current?.animateCamera({
      center: location, zoom:18 });
  }

const handleMapPress = async (event: any) => {
    const { latitude, longitude } = event.nativeEvent.coordinate;
    
    try {
        const location = await Location.reverseGeocodeAsync({ latitude, longitude });
        const address = formatAddress(location as LocationData[]);
        setMarkerLocation({ latitude, longitude });
        if (onMarkerPress) {
            onMarkerPress(latitude, longitude, address);
          }
        
    } catch (error) {
        console.error('Error al obtener la dirección:', error);
    }
};


const formatAddress = (location: LocationData[]) => {
    const { city, street, name, postalCode, region, country } = location[0];
    return `${name || street || city || region || postalCode || country}`;
};

  return (
    <View style={styles.container}>
        <View style={styles.containerMap}>
      <MapView
       ref={(map)=>mapRef.current= map!}
        provider={PROVIDER_GOOGLE}
        region={{
          latitude: location.latitude,
          longitude: location.longitude,
          latitudeDelta: 0.9,
          longitudeDelta: 0.9,
        }}
        style={{ width: "100%", height: 200 , borderRadius:16}}
         onPress={handleMapPress}
      >
         {markerLocation && (
            <Marker
              coordinate={markerLocation}
              title="punto"
              description="Mi Ubicación"
            >
              <CircleBtn
                icon={<Feather name="map-pin" size={24} color={WHITE} />}
                styleBtn={{ backgroundColor: CARDHOME, width:40,height:40}}
              />
            </Marker>
          )}
      </MapView>
      <FAB
        icon={<Feather name="compass" size={24} color={WHITE} />}
        onPress={()=> navigationCamera()}
        styleBtn={{bottom:20, right:20}}
        />
        </View>
    </View>
  );
};

export default MapCreatePlace;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderRadius: 24,
        backgroundColor: "cyan",
      },
      containerMap: {
        width: "100%",
        height: 200,
        borderRadius: 24,
        overflow: "hidden",
        borderColor: PRIMARY_COLOR,
        borderWidth:0.6
      },
});
