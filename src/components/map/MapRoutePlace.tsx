import { StyleSheet, View } from "react-native";
import React, { useEffect, useRef, useState } from "react";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import * as Location from "expo-location";
import CircleBtn from "../buttons/CircleBtn";
import { CARDHOME, PRIMARY_COLOR, WHITE } from "../../theme/globalStyles";
import { Entypo } from "@expo/vector-icons";
import { Feather } from "@expo/vector-icons";
import MapViewDirections from "react-native-maps-directions";
import FAB from "../buttons/FAB";
import { useAuthPlace } from "../../presentation/useAuthPlace";

interface Props {
  lat: number;
  lng: number;
  namePlace?: string;
  adressPlace?: string;
}

const MapRoutePlace = ({ lat = 0, lng = 0, namePlace, adressPlace }: Props) => {
  const { user } = useAuthPlace();
  const [location, setLocation] = useState({ latitude: 0, longitude: 0 });
  const mapRef = useRef<MapView>();
  useEffect(() => {
    navigationCamera();
    getPermitionLocation();
    getUserLocation();
  }, []);

  const getPermitionLocation = async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      return;
    }
  };

  const getUserLocation = async () => {
    let location = await Location.getCurrentPositionAsync({});
    setLocation(location.coords);
  };

  const navigationCamera = async () => {
    if (!mapRef.current) return;
    mapRef.current?.animateCamera({
      center: location,
      zoom: 13.5,
    });
  };

  const GOOGLE_API_KEY = "AIzaSyBQ1GqVWCmfaDf-pZyA5YbrcFqKA1WEg9w";
  const destination = { latitude: lat, longitude: lng };
  const origin = { latitude: location.latitude, longitude: location.longitude };

  return (
    <View style={styles.container}>
      <View style={styles.containerMap}>
        <MapView
          ref={(map) => (mapRef.current = map!)}
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          region={{
            latitude: location.latitude,
            longitude: location.longitude,
            latitudeDelta: 0.9,
            longitudeDelta: 0.9,
          }}
        >
          <Marker
            coordinate={{
              latitude: location.latitude,
              longitude: location.longitude,
            }}
            title={user?.user}
            description="Mi Ubicación"
          >
            <CircleBtn
              icon={<Feather name="map-pin" size={24} color={WHITE} />}
              styleBtn={{ backgroundColor: CARDHOME }}
            />
          </Marker>

          {destination.latitude !== 0 &&
          destination.longitude !== 0 &&
          origin.latitude !== 0 &&
          origin.longitude !== 0 ? (
            <MapViewDirections
              origin={origin}
              destination={{
                latitude: lat,
                longitude: lng,
              }}
              apikey={GOOGLE_API_KEY}
              strokeWidth={3}
              strokeColor={PRIMARY_COLOR}
            />
          ) : undefined}

          <Marker
            coordinate={{
              latitude: lat,
              longitude: lng,
            }}
            title={namePlace}
            description={adressPlace}
          >
            <CircleBtn
              icon={<Entypo name="shop" size={24} color={WHITE} />}
              styleBtn={styles.markerBtn}
            />
          </Marker>
        </MapView>
        <FAB
          icon={<Feather name="compass" size={24} color={WHITE} />}
          onPress={() => navigationCamera()}
          styleBtn={{ bottom: 20, right: 20 }}
        />
      </View>
    </View>
  );
};

export default MapRoutePlace;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 24,
    backgroundColor: "cyan",
  },
  containerMap: {
    width: "100%",
    height: 280,
    borderRadius: 24,
    overflow: "hidden",
  },
  map: {
    width: "100%",
    height: "100%",
    ...StyleSheet.absoluteFillObject,
  },
  markerBtn: {
    backgroundColor: PRIMARY_COLOR,
  },
});
