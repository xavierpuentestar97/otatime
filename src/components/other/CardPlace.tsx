import { Image, StyleSheet, Text, TouchableOpacity, View, ViewStyle } from 'react-native'
import React from 'react'
import { BACKGROUND, PRIMARY_COLOR, SECONDARY_COLOR, globalStyles } from '../../theme/globalStyles'
import { Entypo } from '@expo/vector-icons';
interface Props {
    onPressCard?: () => void
    title?: string
    styleCard?: ViewStyle
    imagePlace?: string
}

const CardPlace = ({ onPressCard, title, styleCard, imagePlace }: Props) => {
    return (
        <View style={{...styles.container, ...styleCard}}>
            <TouchableOpacity style={styles.btnImage} onPress={onPressCard}>
                <Image
                    style={styles.image}
                    source={imagePlace ? { uri: imagePlace } : require('../../assets/images/defaultImage.jpg')}
                />
                <View style={{...styles.cardText}}>
                    <Text style={styles.titleText}>{title}</Text>
                    <Entypo name="chevron-with-circle-right" size={28} color={SECONDARY_COLOR} />
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default CardPlace

const styles = StyleSheet.create({
    container: {
        width: 225,
        height: 200,
        backgroundColor: SECONDARY_COLOR,
        borderRadius: 16,
        marginHorizontal:5
    },
    btnImage: {
        width: '100%',
        height: 200,
        backgroundColor: PRIMARY_COLOR,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    cardText: {
        // width: '85%',
        height: 40,
        backgroundColor: BACKGROUND,
        position: 'absolute',
        bottom: 10,
        borderRadius: 24,
        opacity: 0.8,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingHorizontal: 10,
    },
    image: {
        width: '100%',
        height: 200,
        borderRadius: 16,
    },
    titleText: {
        fontSize: 16,
        ...globalStyles.Text,
        color: SECONDARY_COLOR,
        paddingHorizontal: 5
    },
})