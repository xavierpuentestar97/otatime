import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { INPUT, PRIMARY_COLOR, SECONDARY_COLOR, WHITE, globalStyles } from '../../theme/globalStyles'
import StarsCalifications from './StarsCalifications'
import { AntDesign } from '@expo/vector-icons';

interface Props{
userComment: string
lastName: string
comment: string
califications?: (value: number) => void
userCalification?: number
}

const CardComment = ({userComment, comment, califications, lastName, userCalification}:Props) => {
    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row'}}>
            <View style={styles.icon}>
            <AntDesign name="message1" size={24} color={WHITE} />
            </View>
            <View style={styles.userName}>
                <Text style={styles.textUser}>{userComment} {lastName}</Text>
            </View>
            <View style={styles.stars}>
                <StarsCalifications 
                starCalification={(value: number) => califications} 
                userCalification={userCalification}
                disable
                />
            </View>
            </View>
            <View style={styles.commet}>
            <Text style={styles.textComment}>{comment}</Text>
            </View>
        </View>
    )
}

export default CardComment

const styles = StyleSheet.create({
    container: {
        width: '100%',
        // height: 100,
        backgroundColor: INPUT,
        borderRadius: 24,
        alignItems: 'flex-start',
        marginVertical: 8
    },
    icon: {
        width: 38,
        height: 38,
        backgroundColor: PRIMARY_COLOR,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal:10,
        marginTop:8
    },
    userName:{
        marginTop:15,
        flex:1
    },
    stars:{
        marginTop:10,
        marginHorizontal:3
    },
    commet:{
        width:'70%',
        alignSelf:'center',
        marginBottom:15,
        marginTop:5,
    },
    textUser:{
        fontSize: 20,
       ...globalStyles.TitleSecondary,
        color: SECONDARY_COLOR,
    },
    textComment:{
        fontSize: 15,
       ...globalStyles.Text2,
    }
})