import { FlatList, StyleSheet, TouchableOpacity, View } from "react-native"
import React, { useState } from "react"
import { AntDesign } from '@expo/vector-icons'
import { PRIMARY_COLOR } from "../../theme/globalStyles"

interface Props {
    starCalification: (value: number) => void
    userCalification?: number
    disable?: boolean
}

interface ItemData {
    rate: number;
}

const DATA: ItemData[] = [
    { rate: 1 },
    { rate: 2 },
    { rate: 3 },
    { rate: 4 },
    { rate: 5 },
];



const StarsCalifications = ({ starCalification,userCalification,disable }: Props) => {
   
   
    const [selectedStar, setSelectedStar] = useState<number>(userCalification?userCalification:0);
    return (
        <View>
            <FlatList
                data={DATA}
                renderItem={
                    ({ item }) => (
                        <TouchableOpacity style={styles.item} onPress={() => {
                            starCalification(item.rate)
                            setSelectedStar(item.rate)
                        }}
                        disabled={disable}
                        >
                            <AntDesign name={item.rate <= selectedStar ? 'star' : 'staro'} size={25} color={PRIMARY_COLOR} />
                        </TouchableOpacity>
                    )}
                horizontal
            />
        </View>
    );
};

export default StarsCalifications;

const styles = StyleSheet.create({
    item: {
        width: 30,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 2,
    },
});
