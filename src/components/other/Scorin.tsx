import { StyleSheet, Text, View } from "react-native";
import React from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { globalStyles, PRIMARY_COLOR, SECONDARY_COLOR } from "../../theme/globalStyles";
import { AntDesign } from '@expo/vector-icons'

interface Props{
    scoreText?: number
}

const Scorin = ({scoreText}:Props) => {
  return (
    <View style={styles.container}>
     <View style={{marginRight:6}}>
      <Text style={styles.textScore}>{scoreText}</Text>
     </View>
     <TouchableOpacity style={styles.btn}>
      <AntDesign name={'star'} size={15} color={PRIMARY_COLOR} />
      </TouchableOpacity>
      
    </View>
  );
};

export default Scorin;

const styles = StyleSheet.create({
    container:{
        width: "100%",
        alignItems: "center",
        flexDirection:'row',
        justifyContent:'flex-start',
        
    },
    btn:{
        width: 17,
        height: 17,
        justifyContent: 'center',
    },
    textScore:{
        fontSize: 17,
        ...globalStyles.Text2,
        color: SECONDARY_COLOR,
    }
});
