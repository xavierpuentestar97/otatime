import { Image, ImageStyle, StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native'
import React from 'react'
import { CARDHOME, PLACEHOLDER, WHITE, globalStyles } from '../../theme/globalStyles'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import FAB from '../buttons/FAB';
import { FontAwesome } from "@expo/vector-icons";

interface Props {
    onPressCard?: () => void
    title?: string
    styleCard?: ViewStyle
    categoryText?: string
    directionText?: string
    dateText?: boolean
    styleTitle?: TextStyle
    styleImg?: ImageStyle
    imageSource?: string 
    onPressBtn?: () => void
    textLatitud?: number,
    textLongitud?: number    
}

const CardBtnPlace = ({ onPressCard,textLatitud,textLongitud, title, styleCard, directionText, dateText,styleTitle, styleImg, imageSource, onPressBtn }: Props) => {
    return (
        <View style={{...styles.container,...styleCard}}>
            <TouchableOpacity style={styles.btnContainer} onPress={onPressCard}>
                <View style={styles.conatinerText}>
                    <Text style={{...styles.titleText,...styleTitle}}>{title}</Text>
                    {dateText ?
                        <View style={{justifyContent:'center'}}>
                            <View style={styles.containerDates}>
                            <MaterialCommunityIcons name="scatter-plot-outline" size={15} color={PLACEHOLDER} />
                            <View>
                            <Text style={styles.text}>{`${textLatitud}`}</Text>
                            <Text style={styles.text}>{`${textLongitud}`}</Text>
                            </View>
                            </View>
                            <View style={styles.containerDates}>
                                <Feather name="map-pin" size={15} color={PLACEHOLDER} />
                                <Text style={styles.text}>{directionText}</Text>
                            </View>
                        </View>
                        : <View />}
                </View>
                <Image
                    source={imageSource ? { uri: imageSource } : require('../../assets/images/defaultImage.jpg')}
                    style={{...styles.img, ...styleImg}}
                    
                />
            </TouchableOpacity>
            <FAB 
             icon={<FontAwesome name="trash" size={24} color={WHITE} />}
            styleBtn={{width: 40,height:40,right:-5, top: -5, backgroundColor: 'red' }}
            onPress={onPressBtn}
            />
        </View>
    )
}

export default CardBtnPlace

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 150,
        backgroundColor: CARDHOME,
        borderRadius: 24,
        alignItems: 'center',
    },
    btnContainer: {
        width: '100%',
        height: 150,
        borderRadius: 16,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    img: {
        width: 150,
        height: 150,
        borderRadius: 24,
    },
    conatinerText: {
        flex: 1,
        paddingHorizontal:20,
    },
    titleText: {
        fontSize: 30,
        ...globalStyles.TitleSecondary,
        color: WHITE,
        textAlign: 'center',
    },
    text: {
        fontSize: 15,
        ...globalStyles.Text2,
        color: PLACEHOLDER,
        paddingHorizontal:5
    },
    containerDates:{
        flexDirection: 'row', 
        marginTop:3,
        width:142,
    }
})