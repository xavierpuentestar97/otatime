import { StyleSheet, Text, View } from 'react-native'
import React, { useState } from 'react'
import { INPUT, PLACEHOLDER, PRIMARY_COLOR, SECONDARY_COLOR, WHITE, globalStyles } from '../../theme/globalStyles'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { FontAwesome5 } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';

interface Props{
    onPress?: () => void
    userText?: string
    textCountry?: string
    textMail?: string
    stateUser?: boolean
}


const CardStateUser = ({onPress, userText, textCountry,textMail, stateUser}:Props) => {
    

    
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.containerBtn} onPress={onPress}>
                <View style={styles.contentItem}>
                    <View style={{...styles.icon,  backgroundColor: stateUser ? PRIMARY_COLOR : SECONDARY_COLOR}}>
                        <FontAwesome5 name="user" size={50} color={WHITE} />
                    </View>
                    <View style={styles.contentText}>
                        <Text style={styles.userText}>{userText}</Text>
                        <View style={styles.iconText}>
                            <AntDesign name="flag" size={14} color={PLACEHOLDER} />
                            <Text style={styles.textDates}>{textCountry}</Text>
                        </View>
                        <View style={styles.iconText}>
                            <MaterialIcons name="alternate-email" size={14} color={PLACEHOLDER} />
                            <Text style={styles.textDates}>{textMail}</Text>
                        </View>
                    </View>
                </View>
                <View style={{...styles.stateUser, backgroundColor: stateUser ? PRIMARY_COLOR : SECONDARY_COLOR }}>
                <Text style={styles.textState}>
                        {stateUser ? "Activo" : "Inactivo"}
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default CardStateUser

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 150,
        borderRadius: 24,
        marginBottom:15
    },
    containerBtn: {
        width: '100%',
        height: 150,
        backgroundColor: INPUT,
        borderRadius: 24,
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        width: 81,
        height: 81,
        borderRadius: 50,
        backgroundColor: PRIMARY_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentText: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: 15
    },
    contentItem: {
        width: '90%',
        borderRadius: 24,
        flexDirection: 'row'
    },
    userText: {
        fontSize: 20,
        ...globalStyles.Text,
        color: SECONDARY_COLOR,
    },
    iconText: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 3
    },
    textDates: {
        fontSize: 14,
        ...globalStyles.Text2,
        color: PLACEHOLDER,
        marginLeft: 5
    },
    stateUser:{
        width: '50%',
        height: 30,
        borderRadius: 24,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: PRIMARY_COLOR,
        marginTop:5
    },
    textState:{
        fontSize: 16,
        ...globalStyles.Text,
        color: WHITE,
    }
})