import { StyleSheet } from "react-native";

export const PRIMARY_COLOR = "#17B39E";
export const SECONDARY_COLOR = "#102A34";
export const BACKGROUND = "#F2F8F6";
export const INPUT = "#D2E8E2";
export const PLBUTTON = "#D9D9D9";
export const TERCIARY_COLOR = "#485154";
export const WHITE = "#FFFFFF";
export const BLACK = "#000000";
export const PLACEHOLDER = "#616C70";
export const CARDHOME = "#2F3C6B";


export const globalStyles = StyleSheet.create({
  Title: {
    fontFamily: "RobotoBlack",
  },
  TitleSecondary: {
    fontFamily: "RobotoBold",
  },
  Text: {
    fontFamily: "RobotoMedium",
  },
  Text2: {
    fontFamily: "RobotoRegular",
  },
  Text3: {
    fontFamily: "RobotoThin",
  },
});
