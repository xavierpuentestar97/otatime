import { create } from "zustand";
import { UserSingIn } from "../domain/entities/userSingIn";
import { RegisterUser } from "../actions/auth/singIn";

export interface CreateUser{
    user?: UserSingIn

    createUser: (name: string, last_name: string, country_id: number, age: number, mail:string, password:string, usertype_id: number, state: boolean) => Promise<boolean>
}

export const useCreateUser = create<CreateUser>()((set,get)=>({
    
    createUser: async(name: string, last_name: string, country_id: number, age: number, mail:string, password:string, usertype_id: number, state: boolean)=>{
        const resp = await RegisterUser(name, last_name, country_id, age, mail, password, usertype_id, state)
        if(!resp){
            return false
        }
        set ({user:resp.user})
        return true
    }
}))