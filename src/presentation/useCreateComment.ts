import { create } from "zustand";
import { PostComment } from "../domain/entities/createComment";
import { RegisterComment } from "../actions/comment/postComment";


export interface MessageComment{
    comment?: PostComment
    menssageComment: (content: string, user_id: number, rate: number, date: Date, place_id: number) => Promise<boolean>
}

export const useCreateComment = create<MessageComment>()((set,get)=>({
    menssageComment: async(content: string, user_id: number, rate: number, date: Date, place_id: number) =>{
        const resp = await RegisterComment(content, user_id, rate, date, place_id)
        if(!resp){
            return false
        }
        set ({comment:resp.comment})
        return true
    
    }

}))