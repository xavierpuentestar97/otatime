import { StyleSheet, Text, View } from 'react-native'
import React,{PropsWithChildren, useEffect} from 'react'
import { useNavigation } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'
import { useAuthPlace } from '../useAuthPlace'

export const AuthProvider = ({children}:PropsWithChildren) => {
    const navigation = useNavigation<StackNavigationProp<any,any>>();
    const {checkStatus,status} = useAuthPlace();

    useEffect(() => {
    checkStatus();
    }, [])

    useEffect(() => {
      if(status  !== 'checking'){
        if(status === 'authenticated'){
            navigation.reset({
                index: 0,
                routes: [{name: 'TabsNavigator'}]
            })
        }else{
            navigation.reset({
                index: 0,
                routes: [{name: 'LoginScreen'}]
            })
        }

      }
    }, [status])
    
  return (
    <>
    {children}
    </>
  )
}

