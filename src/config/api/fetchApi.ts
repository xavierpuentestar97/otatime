import axios from "axios";

export const API_HOST = "https://otatime-production-c141.up.railway.app/api";

const fetchApi = axios.create({
    baseURL: API_HOST,
    headers: {
        'Content-Type': 'application/json'
    }
});

export { fetchApi };