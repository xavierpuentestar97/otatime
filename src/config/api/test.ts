import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";


export const API_HOST = "https://otatime-production-c141.up.railway.app/api"


const testApi = axios.create({
    baseURL: API_HOST,
    headers:{
        'Content-Type': 'application/json'
    }
})

testApi.interceptors.request.use(
    async(config)=>{
        const token = await AsyncStorage.getItem('token')
        if(token){
            config.headers['Authorization'] = `Bearer ${token}`
        }

        return config
    }
)

export {
    testApi,    
}