export type PermissionSatus =
  | "granted"
  | "denied"
  | "bloked"
  | "limited"
  | "unavailable"
  | "undetermined";
