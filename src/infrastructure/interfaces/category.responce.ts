
export interface CategoryResponse {
    id_category:   number;
    name_category: string;
    image:         string;
}
