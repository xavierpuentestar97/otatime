import { Country } from "../../domain/entities/country";
import type { CountryResponse } from "../interfaces/country.response";


export class CountryMapper{

    static countryUsersToEntity (countryResponse: CountryResponse): Country{

        return{
            id_country: countryResponse.id_country,
            name_country: countryResponse.name_country,
        }
    }

}