import { Place } from "../../domain/entities/place";
import { PlaceResponse } from "../interfaces/places.response";

export class PlaceMapper {
  static PlaceUsersToEntity(placeResponse: PlaceResponse): Place {
    return {
      id_place: placeResponse.id_place,
      name_place: placeResponse.name_place,
      description: placeResponse.description,
      description_en: placeResponse.description_en,
      image: placeResponse.image,
      address: placeResponse.address,
      lat: placeResponse.lat,
      lng: placeResponse.lng,
      category_id: placeResponse.category_id,
      average_rating: placeResponse.average_rating,
    };
  }
}
