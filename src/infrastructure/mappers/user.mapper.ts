
import { UserUpdate } from "../../domain/entities/userUpdate";
import { UserResponse } from "../interfaces/user.responser";


export class UserMapper {
    static UsersToEntity(userResponse: UserResponse): UserUpdate {
      return {
        id_user: userResponse.id,
        name: userResponse.name,
        last_name: userResponse.last_name,
        country_id: userResponse.country_id,
        age: userResponse.age,
        mail: userResponse.mail,
        password: userResponse.password,
        usertype_id: userResponse.usertype_id,
        state: userResponse.state,
      };
    }
  }