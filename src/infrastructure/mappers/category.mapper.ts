import { Category } from "../../domain/entities/category";
import { CategoryResponse } from "../interfaces/category.responce";



export class CategoryMapper {
    static categoryUsersToEntity(categoryResponse: CategoryResponse): Category{

        return {
            id_category: categoryResponse.id_category,
            name_category: categoryResponse.name_category,
            image: categoryResponse.image,
        }
    }
}