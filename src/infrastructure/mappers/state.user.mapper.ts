import { UserUpdate } from "../../domain/entities/userUpdate";
import { StateResponse } from "../interfaces/state.response";



export class UserStateMapper {
    static StateUsersToEntity(userStateResponse: StateResponse): UserUpdate {
      return {
        id_user: userStateResponse.id_user,
        name: userStateResponse.name,
        last_name: userStateResponse.last_name,
        country_id: userStateResponse.country_id,
        age: userStateResponse.age,
        mail: userStateResponse.mail,
        password: userStateResponse.password,
        usertype_id: userStateResponse.usertype_id,
        state: userStateResponse.state,
      };
    }
  }