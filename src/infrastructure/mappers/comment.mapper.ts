import { CommentUser } from "../../domain/entities/comment";
import { CommentResponse } from "../interfaces/comment.responce";

export class CommentMapper {
  static CommentUsersToEntity(commetResponse: CommentResponse): CommentUser {
    return {
      id_comment: commetResponse.id_comment,
      content: commetResponse.content,
      user_id: commetResponse.user_id,
      rate: commetResponse.rate,
      date: commetResponse.date,
      place_id: commetResponse.place_id,
      user_name: commetResponse.user_name,
      user_last_name: commetResponse.user_last_name,
      name_place: commetResponse.name_place,
      id_place: commetResponse.id_place,
    };
  }
}
