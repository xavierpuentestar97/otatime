import { testApi } from "../../config/api/test";
import { PostComment } from "../../domain/entities/createComment";
import { CommentResponse } from "../../infrastructure/interfaces/comment.responce";


const returnComment = (data: CommentResponse) =>{
const comment: PostComment = {
    id_comment: data.id_comment,
    content: data.content,
    user_id: data.user_id,
    rate: data.rate,
    date: data.date,
    place_id: data.place_id,
}
return{
    comment: comment
}
}

export const RegisterComment = async(content: string, user_id: number, rate: number, date: Date, place_id: number) => {
try {
    const {data} = await testApi.post<CommentResponse>('/comment/',
    {
        content: content,
        user_id: user_id,
        rate: rate,
        date: date,
        place_id: place_id,
    }
)
return returnComment(data)
} catch (error) {
    return null;
}
}