import { testApi } from "../../config/api/test";
import { CommentResponse } from "../../infrastructure/interfaces/comment.responce";
import { CommentMapper } from "../../infrastructure/mappers/comment.mapper";

export const getCommentById = async (
  id: number,
  page: number,
  limit: number = 3
) => {
  try {
    const { data } = await testApi.get<CommentResponse[]>(
      `/commentPlaces/${id}?page=${page}&limit=${limit}`
    );
    const comment = data.map(CommentMapper.CommentUsersToEntity);
    return comment;
  } catch (error) {
    console.log("🚀 ~ getCommentById ~ error:", error);
    throw new Error("Error al mostrar");
  }
};
