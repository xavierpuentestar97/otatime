import { fetchApi } from "../../config/api/fetchApi"
import { UserSingIn } from "../../domain/entities/userSingIn"
import { UserResponse } from "../../infrastructure/interfaces/user.responser"


const returnUser = (data: UserResponse) => {
    const user : UserSingIn = {
        id: data.id,
        mail: data.mail,
        name: data.name,
        last_name: data.last_name,
        password: data.password,
        usertype_id: data.usertype_id,
        state: data.state,
        age : data.age,
        country_id: data.country_id,
    }
    return{
        user: user,
    }
}

export const RegisterUser = async(name: string, last_name: string, country_id: number, age: number, mail:string, password:string, usertype_id: number, state: boolean ) =>{

    mail = mail.toLowerCase();
    try {
        const {data} = await fetchApi.post<UserResponse>('/user/',
            {
                name: name,
                last_name: last_name,
                country_id: country_id,
                age: age,
                mail: mail,
                password: password,
                usertype_id: usertype_id,
                state: state,
            }
        )
        return returnUser(data)
    } catch (error) {
      
        return null;
    }


}