import { testApi } from "../../config/api/test";
import { User } from "../../domain/entities/user";
import type { AuthResponse } from "../../infrastructure/interfaces/auth.responce";

const returnUsertToken = (data: AuthResponse) =>{

    const user : User = {
        id: data.id,
        mail: data.mail,
        user: data.user,
        password: data.password,
        usertype_id: data.usertype_id,
        state: data.state,
    }
    return{
        user: user,
        token: data.token
    }
}

export const authLogin = async(mail:string, password:string) => {

    mail = mail.toLowerCase();
    try {
        const {data} = await testApi.post<AuthResponse>('/user/login',
            {
                mail: mail,
                password: password
            }
        )
        return returnUsertToken(data)
    } catch (error) {
      
        return null;
    }
}

export const authCheckStatus = async()=>{
try {
    const {data} = await testApi.post<AuthResponse>('/user/login')
    
    return returnUsertToken(data)
} catch (error) {
   
    return null
}
}

