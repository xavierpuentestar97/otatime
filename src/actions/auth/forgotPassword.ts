import { fetchApi } from "../../config/api/fetchApi";


export const sendEmail = async (mail: string) => {
    try {
      const data = await fetchApi.get(`/user/forgot-password/${mail}`);
      if (data.status === 200) {        
        return true; // Indica que el correo electrónico se envió con éxito
      } else {
        return false; // Indica que hubo un problema al enviar el correo electrónico
      }
    } catch (error) {
      // console.error("Error sending email:", error);
      return false; // Indica que hubo un error al enviar la solicitud POST
    }
  };