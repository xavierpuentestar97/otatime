import { fetchApi } from "../../config/api/fetchApi"
import { testApi } from "../../config/api/test"
import { Category } from "../../domain/entities/category"
import { CategoryResponse } from "../../infrastructure/interfaces/category.responce"
import { CategoryMapper } from "../../infrastructure/mappers/category.mapper"


export const getCategories = async(): Promise<Category[]> =>{

    try {
        const {data} = await fetchApi.get<CategoryResponse[]>(`/category`)
        const categories =  data.map( CategoryMapper.categoryUsersToEntity)
        return (categories)
    } catch (error) {
        console.log("🚀 ~ categories ~ error:", error)
        throw new Error("Error al mostrar");
    }
}
