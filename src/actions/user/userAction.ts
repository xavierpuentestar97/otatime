import { fetchApi } from "../../config/api/fetchApi"
import { StateUpdate } from "../../domain/entities/stateUpdate"
import { UserUpdate } from "../../domain/entities/userUpdate"
import { StateResponse } from "../../infrastructure/interfaces/state.response"
import { UserResponse } from "../../infrastructure/interfaces/user.responser"
import { UserStateMapper } from "../../infrastructure/mappers/state.user.mapper"
import { UserMapper } from "../../infrastructure/mappers/user.mapper"


export const getUserById = async(id: number): Promise<UserUpdate[]> =>{
    try {
        const {data} = await fetchApi.get<UserResponse[]>(`/user/${id}`)
        const user =  data.map( UserMapper.UsersToEntity)
        // console.log("🚀 ~ updateUser ~ user:", user)
        return (user)
    } catch (error) {
        throw new Error("Error al mostrar");
    }
}

export const updateUserById = async(id: number ,updatedUserData: UserUpdate) =>{
    try {
       await fetchApi.patch(`/user/${id}`, updatedUserData)
    } catch (error) {
        console.log("🚀 ~ updateUser ~ error:", error)
        throw new Error("Error al mostrar");
    }
}

export const updateUserByState = async(state: number) =>{
    try {
        const {data} = await fetchApi.get<StateResponse[]>(`/stateUserGet/${state}`)
        const user =  data.map( UserStateMapper.StateUsersToEntity)
         return (user)
    } catch (error) {
        console.log("🚀 ~ updateUser ~ error:", error)
        throw new Error("Error al mostrar");
    }
}

export const updateUserState = async(id: number ,updatedUserData: StateUpdate) =>{
    try {
       await fetchApi.patch(`/user/userState/${id}`, updatedUserData)
    } catch (error) {
        console.log("🚀 ~ updateUser ~ error:", error)
        throw new Error("Error al mostrar");
    }
}