import { fetchApi } from "../../config/api/fetchApi";
import { Country } from "../../domain/entities/country";
import type { CountryResponse } from "../../infrastructure/interfaces/country.response";
import { CountryMapper } from "../../infrastructure/mappers/country.mapper";



export const getCountryDates = async(): Promise<Country[]>=>{

    try {
        const {data} = await fetchApi.get<CountryResponse[]>(`/country`)
        const countries =  data.map( CountryMapper.countryUsersToEntity)
        return (countries)
    } catch (error) {
        console.log("🚀 ~ getCountryDates ~ error:", error)
        throw new Error("Error al mostrar");
    }
}