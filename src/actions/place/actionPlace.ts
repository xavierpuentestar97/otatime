
import { testApi } from "../../config/api/test";
import { PlaceResponse } from "../../infrastructure/interfaces/places.response";
import { BASE_URL } from "../../service/hooks/config";

export const PlaceCreateOn = async (placeData: FormData) => {
    try {
      const { data } = await testApi.post<PlaceResponse>(
        `${BASE_URL}api/place/uploadImage`,
        placeData,
        {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        },
      )
      return data
    } catch (error: any) {
      return error.response.data
    }
  }

export const PlaceDetele = async (placeId: number) =>{
  try {
     await testApi.delete<PlaceResponse[]>(`/place/${placeId}`)
     return true
  } catch (error) {
    throw new Error("Error al mostrar");
  }
}

export const updatePlaceById = async(id: number ,placeData: FormData) =>{
  try {
     const { data } = await testApi.patch<PlaceResponse>(
      `${BASE_URL}api/place/updatePlace/${id}`,
      placeData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    )
    return data
  } catch (error) {
      console.log("🚀 ~ updateUser ~ error:", error)
      throw new Error("Error al mostrar");
  }
}

