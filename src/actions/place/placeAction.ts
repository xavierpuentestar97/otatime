import { testApi } from "../../config/api/test"
import { Place } from "../../domain/entities/place"
import { CommentResponse } from "../../infrastructure/interfaces/comment.responce"
import { PlaceResponse } from "../../infrastructure/interfaces/places.response"
import { CommentMapper } from "../../infrastructure/mappers/comment.mapper"
import { PlaceMapper } from "../../infrastructure/mappers/place.mapper"


export const getPopularPlaces = async(): Promise<Place[]>=>{
    try {
        const {data} = await testApi.get<PlaceResponse[]>(`/popularPlaces`)
        const places =  data.map( PlaceMapper.PlaceUsersToEntity)
        return (places)
    } catch (error) {
       console.log("🚀 ~ getPopularPlaces ~ error:", error)
        throw new Error("Error al mostrar");
    }
}

export const getPlaces = async(): Promise<Place[]>=>{
    try {
        const {data} = await testApi.get<PlaceResponse[]>(`/place`)
        const places =  data.map( PlaceMapper.PlaceUsersToEntity)
        return (places)
    } catch (error) {
       console.log("🚀 ~ getPlaces ~ error:", error)
        throw new Error("Error al mostrar" );
    }
}

export const getByCategoryPlaces = async()=>{
    try {
        const {data} = await testApi.get<PlaceResponse[]>(`/categoryPlaces`)
        const places =  data.map( PlaceMapper.PlaceUsersToEntity)
        return (places)
    } catch (error) {
        console.log("🚀 ~ getByCategoryPlaces ~ error:", error)
        throw new Error("Error al mostrar");
    }
}

export const getByCategoryPlacesId = async(categoryId: number)=>{
    try {
        const {data} = await testApi.get<PlaceResponse[]>(`/categoryPlaces/${categoryId}`)
        const places =  data.map( PlaceMapper.PlaceUsersToEntity)        
        return (places)
    } catch (error) {
        throw new Error("Error al mostrar");
    }
}

export const getPlacesByName = async(namePlace: string)=>{
    try {
        const {data} = await testApi.get<PlaceResponse[]>(`/namePlaces?name=${namePlace}`)
        const places =  data.map( PlaceMapper.PlaceUsersToEntity)        
        return (places)
    } catch (error) {
        throw new Error("Error al mostrar");
    }
}

export const getAllPlaceComment = async (
    page: number,
    limit: number = 15
  ) => {
    try {
      const { data } = await testApi.get<PlaceResponse[]>(
        `/commentAllPlaces?offset=${page}&limit=${limit}`
      );
      const places = data.map(PlaceMapper.PlaceUsersToEntity);
      return places;
    } catch (error) {
      throw new Error("Error al mostrar");
    }
  };

  export const getCommentByPlace = async (
    id: number,
    page: number,
    limit: number = 10
  ) => {
    try {
      const { data } = await testApi.get<CommentResponse[]>(
        `/commentPlaces/${id}?page=${page}&limit=${limit}`
      );
      const comment = data.map(CommentMapper.CommentUsersToEntity);
      return comment;
    } catch (error) {
      console.log("🚀 ~ getCommentById ~ error:", error);
      throw new Error("Error al mostrar");
    }
  };