import { testApi } from "../../config/api/test";
import { PlaceResponse } from "../../infrastructure/interfaces/places.response";
import { PlaceMapper } from "../../infrastructure/mappers/place.mapper";


export const getPlacesById = async(id: number)=>{
    try {
        const {data} = await testApi.get<PlaceResponse[]>(`/place/${id}`)
        const place =  data.map( PlaceMapper.PlaceUsersToEntity)
        return (place)
    } catch (error) {
        console.log("🚀 ~ getPlacesById ~ error:", error)
        throw new Error("Error al mostrar");
    }
}