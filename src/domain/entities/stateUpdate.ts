export interface StateUpdate {
  id_user: number;
  name: string;
  last_name: string;
  country_id: number,
  age: number,
  mail: string;
  state: boolean;
  }