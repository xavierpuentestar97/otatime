export interface PlaceCreate{
    id_place: number,
    name_place: string,
    description: string,
    imagePath: string,
    address: string,
    lat:number,
    lng: number,
    category_id: number
}