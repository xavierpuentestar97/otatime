export interface Place{
    id_place: number,
    name_place: string,
    description: string,
    description_en: string,
    image: string,
    address: string,
    lat:number,
    lng: number,
    category_id: number
    average_rating: number
}