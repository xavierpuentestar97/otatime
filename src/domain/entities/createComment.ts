export interface PostComment {
    id_comment: number,
    content: string,
    user_id: number,
    rate: number,
    date: Date,
    place_id: number,
}