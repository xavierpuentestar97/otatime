export interface CommentUser {
    id_comment: number,
    content: string,
    user_id: number,
    rate: number,
    date: Date,
    place_id: number,
    user_name: string,
    user_last_name: string,
    name_place: string,
    id_place: number,
}