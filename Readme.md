# Run project
> `yarn install` > `npx expo start` or alternatively `npx expo start --clear`
# libraries/install

### [Stack Navigator](https://reactnavigation.org/docs/stack-navigator/)
### [Reanimated](https://docs.expo.dev/versions/latest/sdk/reanimated/)
### [Tabs Navigator](https://reactnavigation.org/docs/tab-based-navigation/)
### [Bottom Tabs Navigator](https://reactnavigation.org/docs/bottom-tab-navigator)

### [react-native-dropdown-select-list](https://www.npmjs.com/package/react-native-dropdown-select-list)
>yarn add react-native-dropdown-select-list

### [React Native Reanimated](https://www.npmjs.com/package/react-native-dropdown-select-list)
yarn add react-native-reanimated
### [Add Reanimated's babel plugin]
Add react-native-reanimated/plugin plugin to your babel.config.js.

  module.exports = {
    presets: [
      ... // don't add it here
    ],
    plugins: [
      ...
      'react-native-reanimated/plugin',
    ],
  };
### [React Native Reanimated carousel](https://www.npmjs.com/package/react-native-dropdown-select-list)
yarn add react-native-reanimated-carousel
