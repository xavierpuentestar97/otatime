import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, useColorScheme } from 'react-native';
import useFont from './src/service/hooks/useFont';
import { BACKGROUND, PRIMARY_COLOR, WHITE, globalStyles } from './src/theme/globalStyles';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { StackNavigator } from './src/navigator/StackNavigator';
import { SafeAreaView } from 'react-native-safe-area-context';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';



// const Stack = createNativeStackNavigator();

const queryClient = new QueryClient()
export default function App() {
  const { loaded } = useFont()
  if (!loaded) {
    return null
  }
 
  return (
    <QueryClientProvider client={queryClient}>
    <NavigationContainer
    theme={{
      dark : true,
      colors: {
        primary: PRIMARY_COLOR,
        background: BACKGROUND,
        card: BACKGROUND,
        text: WHITE,
        border: WHITE,
        notification: PRIMARY_COLOR
      }
    }}
    >
       <SafeAreaView style={{ flex: 1 }}>
       <StackNavigator />
       </SafeAreaView>
  </NavigationContainer>
  </QueryClientProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text:{
    ...globalStyles.Title,
     fontSize: 20,
  }
});
